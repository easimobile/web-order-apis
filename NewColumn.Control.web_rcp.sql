if not exists (select * from INFORMATION_SCHEMA.columns
               where table_name = 'control'
                     and column_name = 'web_rcp')
begin
    ALTER TABLE "control" ADD web_rcp int null
end
