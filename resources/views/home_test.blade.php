<!DOCTYPE html>
<html>
<head>
  <title>Web Order</title>
  <meta charset="UTF-8">
  <link href="{!! asset('images/favicon.ico') !!}" rel="shortcut icon" type="image/x-icon" />
  <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no" />
  
  <!-- Font Awesome Icons -->
  <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
  
  <!-- Ionicons -->
  <link href="{{ asset('css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
  
  <link href="{{ asset('AdminLTE/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />

  <!--js-->
  <script src="{{ asset('js/jquery.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap.js') }}"></script>

  <!-- jquery ui -->
  <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
  <script src="{{ asset('js/jquery-ui.js') }}"></script>
  
  <!-- jquery validate plugin -->
  <script src="{{ asset('js/jquery.validate.js') }}"></script>

  <!-- Bootstrap 3.3.2 -->
  <link href="{{ asset('AdminLTE/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

  <!-- css -->
  <link rel="stylesheet" href="{{ asset('css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('css/style2.css') }}">
  <link rel="stylesheet" href="{{ asset('css/w3.css') }}">

  <script language="javascript" type="text/javascript" src="{{ asset('js/sha1.js') }}"></script>

  <style>
    /* General CSS */
    html{
      overscroll-behavior: contain;
    }

    tbody tr td:first-child {
      width: 240px;
      max-width: 230px;
      word-break: break-all;
      padding: 10px 0;
      font-size: 17px;
    }

    input[type=checkbox] {
      transform: scale(1.2);
    }

    input[type=number] {
      line-height: 27px;
      height: 40px;
      margin-top: 10px;
      width: 100px;
      font-size: 18px;
      padding-left: 10px;
    }

    input[type=number]::-webkit-inner-spin-button {
      width: 50px;
      height: 50px;
      opacity: 1
    }

    .quantity{
      display: flex;
      align-items: center;
      justify-content: center;
      margin-bottom:10px;
    }

    .alert-holdbill {
      background-color: #ffffff;
      color: #777;
      border-color: #000000;
      border-radius: 20px;
      border: solid 2px;
    }

    #alertFlashMessage{
      min-width: auto;
      max-width: 50%;
      position: fixed;
      left: 10px;
      bottom: 20px;
      z-index: 1000;
      font-size: 20px;
      display: none;
    }

    #alertFlashMessageATC{
      max-width: 500px;
      min-width: 300px;
      height: 160px;
      position: fixed;
      text-align: center;
      left: 0;
      right: 0;
      top: 0;
      bottom: 0;
      margin: auto;
      z-index: 1000;
      font-size: 25px;
      padding: 55px;
      font-weight: 600;
      display: none;
    }

    #alertNotificationFlashMessage{
      position: fixed;
      left: 0;
      top: 0;
      right: 0;
      margin: 15px auto;
      border-radius: 10px;
      width: fit-content;
      min-height: 40px;
      padding: 20px;
      display: none;
    }

    .alertOverlay {
      position: fixed;
      display: none;
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: rgba(0,0,0,0.5);
      z-index: 900;
    }

    .orders{
      background-color:#d92e2e;
      border-radius: 15px;
      padding: 4px 8px;
      color: #fff;
      position: absolute;
      display:none;
      margin: -35px 0 0 20px;
    }
    /* End General CSS */

    /* ScrollBar */
    /* width */
    ::-webkit-scrollbar {
      width: 5px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
      background: #f1f1f1; 
    }
    
    /* Handle */
    ::-webkit-scrollbar-thumb {
      background: #888; 
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
      background: #555; 
    }
    /* END ScrollBar */   

    /* Loader */
    #loader {
      position: fixed;
      display: none;
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: rgba(0,0,0,0.5);     
      z-index:999;
    }

    #loader-inner{
      position: absolute;
      left: 50%;
      top: 50%;
      z-index: 1000;
      margin: -75px 0 0 -55px;
      border: 16px solid #f3f3f3;
      border-radius: 50%;
      border-top: 16px solid #3498db;
      width: 100px;
      height: 100px;
      -webkit-animation: spin 2s linear infinite; /* Safari */
      animation: spin 2s linear infinite;
    }

    /* Safari */
    @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }
    /* END Loader */

    /* Navigation Sidebar */
    .menulbl{
      font-family:'Avenir Heavy';
      vertical-align:middle;
      font-size: 25px;
      font-weight: 900;
    }

    #logout_sidebar_button {
      position: absolute;
      display: inline-block;
      bottom: 0;
      left: 0;
      box-shadow: 0 -3px 6px rgba(0, 0, 0, 0.16);
      width: 100%;
      padding: 5px 0;
      font-size: 20px;
      font-weight: 500;
      height: 45px;
      background-color: #fff;
    }

    .sidebar {
      height: 100%;
      position: fixed!important;
      background-color: #fafafa;
      z-index:3;
      width:200px;
      float:left;
    }

    .navTop{
      margin-top:10px;
    }

    .navList{
      display: block;
      position: absolute;
      padding: 0;
      margin: 0;
      list-style-type: none;
      width: -webkit-fill-available;
      float: left;
      font-size: 20px;
      height: 100%;
      padding-bottom: 110px;
      overflow:auto;
    } 

    .sidebar li a{
      color: #5d6174;
      text-decoration:none;
      padding: 5px 30px;
      display:block;
      transition:0.3s;
      cursor: pointer;
    }
      
    .navList li a:hover{
      color:#fff;
      background-color: #e3e3e3;
    }
  
    /* END of Navigation Sidebar */

    /* Cart Sidebar */
    .txtMult td {
      border-bottom: 1pt solid #cabdbd;
    }

    .option{
      height: 50px;
      width: 180px;
      cursor: pointer;
      border-radius: 26px;
      background-image: linear-gradient(180deg, #777b90 0%, #5d6174 100%);
      color: #fff;
      padding: 0;
      border:none;
    }

    .cart-title,.cart-btn{
      display: block;
      width: 100%;
      font-size: 20px;
      padding-bottom:10px;
    }

    .ct-box1{
      display:inline-block;
      width:80%;
    }

    .ct-box2{
      display:inline-block; 
      float:right;
      padding-right:20px;
      width:20%;
    }

    .cb-box1,.cb-box2{
      display:inline-block;
      text-align:center;
      width:40%;
    }

    .cb-box3{
      display:inline-block;
      text-align:center;
      width:20%;
    }
    
    .deletebtn {
      height: 60px;
      width: 50px;
      cursor: pointer;
      background: url('{{asset('images/close.png')}}') no-repeat;
      background-size: 100%;
      padding: 26px;
      border: none;
    }

    .voidBtn {
      background-color:#f70f0f;
      -moz-border-radius:28px;
      -webkit-border-radius:28px;
      border-radius:5px;
      border:1px solid #ff0000;
      display:inline-block;
      cursor:pointer;
      color:#ffffff;
      font-family:Arial;
      text-decoration:none;
      text-shadow:0px 1px 0px #ff0000;
      padding: 10px 30px;
    }

    .voidBtn:hover {
      background-color:#d61a1a;
    }

    .voidBtn:active {
      position:relative;
      top:1px;
    }

    #rightMenu{
      width:550px;
      right:0;
      float:right;
      z-index:3;
      display:none;
      right:0;
      height:100%;
    }

    .cart-cal{
      padding: 20px;
      bottom: 0;
      position: fixed;
      width: -moz-available;          /* WebKit-based browsers will ignore this. */
      width: -webkit-fill-available;  /* Mozilla-based browsers will ignore this. */
      width: fill-available;
      height: 120px;
      left:auto;
      box-shadow: 0 -3px 6px rgba(0, 0, 0, 0.16);
      background-color: #fafafa;
     animation: animateright 0.4s;
    }

    #holdbillbtn{
      position:fixed;
      right:20px;
      bottom:20px;
      width: 150px;
      height: 60px;
      border-radius: 60px;
      background-image: linear-gradient(180deg, #777b90 0%, #5d6174 100%);
      color:white;
      animation: animateright 0.25s;
    }
    
    .cartClose{
      width:100%;
      text-align:left;
      position: fixed;
      height: 45px;
      border: none;
      padding-left: 20px;
      box-shadow: 0px 1px 15px 0px rgba(0, 0, 0, 0.25);
      background-color: #ffffff;
      z-index: 1;
    }

    .fixed-panel{
      padding-left:10px;
      display:none;
      height: 100%;
      margin-top: 45px;
      margin-bottom: 100px;
      overflow-y: auto;
    }
    /* End Cart Sidebar */

    #myBtn {
      cursor: pointer;
      transition: background-color .3s, opacity .5s, visibility .5s;
      opacity: 0;
      visibility: hidden;
    }
    
    #button.show {
      cursor: pointer;
      opacity: 1;
      visibility: visible;
    }

    #myBtn:hover {
      background-color: #555;
    }

    /* Nav Bar */   
    .Overlay2{
      display: none;
    }

    .overlay {
      position: fixed;
      display: none;
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: rgba(0,0,0,0.5);
      z-index: 4;
      cursor: pointer;
    }

    .top-nav-bar{
      width: 100%;
      display: table;
    }

    .menuBtn{
      background: url('{{asset('images/menu.png')}}') no-repeat;
      background-size: 90%; 
      width: 50px;
      height: 60px;
      margin: 20px 30px 0;
      border: none;
      background-color: transparent;
      float:left;
    }

    .tableBtn{
      background: url('{{asset('images/table.png')}}') no-repeat;
      background-size: 90%; 
      width: 50px;
      height: 60px;
      margin: 20px 30px 0;
      border: none;
      background-color: transparent;
      float:right;
    }

    .cartBtn{
      background: url('{{asset('images/cart.png')}}') no-repeat;
      background-size: 90%; 
      width: 50px;
      height: 60px;
      margin: 20px 30px 0;
      border: none;
      background-color: transparent;
      float:right;
    }

    @media (min-width: 993px){
      .menulbl {
        margin: 20px 0;
      }
    
    }
    /* End Nav Bar */

    /* Pax Modal */
    .pax{
      display: block;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%) !important;
      background: #fff;
      padding: 20px;
      border-top: 0;
      color: #666; 
      width: 500px;
    }

    .pax-container{
      display:block;
      width:auto;
      height:auto;
      text-align:center;
    }

    #closepax{
      float: right;
      display: inline-block;
      font-size: 18px;
      background-color: Transparent;
      border: none;
      cursor:pointer;
    }

    #openPriceInput::-webkit-inner-spin-button, 
    #openPriceInput::-webkit-outer-spin-button{ 
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;
      margin: 0; 
      width: 0;
      height: 0;
    }

    #proceed {
    margin: 20px auto;
    }
    /* End Pax Modal */

    /* addToCart Button */
    .button {
      -khtml-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      border: none;
      padding: 8px 16px;
      text-align: center;
      cursor: pointer;
      white-space: nowrap;
      width:150px;
      height: 55px;
    }

    button:disabled{
      cursor: not-allowed;
      opacity: 0.3;
    }
    
    .addToCart{
      display: block;
      margin: 0 auto;
      border-radius: 26px;
      background-image: linear-gradient(180deg, #777b90 0%, #5d6174 100%);
      color:#fff;
      padding:0;
      font-family:'Avenir Heavy';
    }

    .hover-addToCart:hover{
      background-image: linear-gradient(180deg, #545457 0%, #5d6174 100%);
      transition: 0.2s ease;
    }
    /* End addToCart Button */

    /* Remark Modal */
    .remarkModal{
      padding: 0px 35px;
      font-size:16px;
    }

    .remarkModal textarea{
      width: 100%;
    }

    #selectionTitle{
      font-weight:bold;
      padding-bottom:5px;
      text-align:center;
    }

    #option_selection{
      text-align: center;
    }

    .btn {
      border-radius: 30px;
      border: 4px solid #5d6174;
      background-color: #fff;
      color: #5d6174;
      width: 160px;
      height: 50px;
      margin: 5px 10px;
    }
    
    .btn-primary.hover,
    .btn-primary:hover,
    .btn-primary.active:hover,
    .btn-primary:active:hover,
    .primary:hover,
    .primary.active:hover{
      background-color: #5d617485;
      border-color: #5d6174;
    }

    .primary:active,
    .primary.active:active,
    .btn-primary.focus,
    .btn-primary:focus,
    .btn-primary.active,
    .btn-primary:active,
    .btn-primary.active.focus,
    .btn-primary.active:focus,
    .btn-primary.active.active,
    .btn-primary.active:active{
      background-color: #5d6174d1;
      border-color: #5d6174;
    }

    .modal-footer button {
      border-radius: 60px;
      background-image: linear-gradient(180deg, #777b90 0%, #5d6174 100%);
    }
    /* End Remark Modal */

    /*Menu Product*/
    .prod_nm {
      height: 44px;
      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-line-clamp: 2;
      -webkit-box-orient: vertical;
    }

    .menu-title{
      padding: 0 0 10px 0;
      text-align: center;
    }

    .product-container{
      width:33.33333%;
      float:left;
      padding: 10px 5px;
    }

    .image-container{
      width: auto;
      height: 300px;
      overflow:hidden;
      border-top-left-radius: 20px;
      border-top-right-radius: 20px;
      display: flex;
      justify-content: center;
      flex-wrap: wrap;
    }

    .image-container img{
      object-fit: cover;
    }

    .product-content-container{
      padding:10px;
      text-align:center;
    }

    .prod_ch {
      height: 55px;
      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-line-clamp: 2;
      -webkit-box-orient: vertical;
    }

    .p-border{
      box-shadow: 0 5px 6px rgba(0, 0, 0, 0.16);
      border-radius: 20px;
      background-color: #ffffff;
    }

    .p-price{
      padding-bottom:5px;
      text-align:center;
      font-size: 18px;
    }

    .menuAdd,.menuSub,.add,.sub{
      display:inline-block;
      border: none;
      background-image: linear-gradient(180deg, #777b90 0%, #5d6174 100%);
      height: 50px;
      width: 50px;
      border-radius: 25px;
      cursor: pointer;
      font-size: 25px;
      color: #fff;
      text-align:center;
      -webkit-appearance: none;
      padding:0;
    }

    .menu_quantity, .val2{
      border: none;
      width: 50px;
      height: 50px;
      text-align: center;
    }
    /*End Menu Product*/

    /*Open Price Modal*/
    .openPriceModal{
      text-align:center;
    }

    #openPriceInput{
      padding: 10px;
      font-size: 20px;
      width: 150px;
    }
    /*End Open Price Modal*/
  </style>
</head>
<body class="w3-light-grey w3-content" style="max-width:1600px">
<div id="loader" style="display:none;">
  <div id="loader-inner"></div>
</div>
<!-- Cart -->
<div class="w3-sidebar w3-bar-block w3-card-2 w3-animate-right" id="rightMenu">
  <button onclick="closeRightMenu()" class="w3-large cartClose">Close &times;</button>
	<form id="checkoutForm" name="checkoutForm" method="post">
    <div class="fixed-panel">
      <table id="cartTbl" cellspacing="0">
      </table>     
    </div> 
    <div class="cart-cal">
      <div class="subTotal" style="font-size:20px;">
        <label for="subTotal" style="display:inline-block;width:150px;">Sub Total: </label>
        <label name="subTotal" id="subTotal" style="display:inline-block;text-align:right;width:150px;">$ 0.00</label>
        <input type='hidden' name='subTotal'  class="subTotal" value='0.00' id='subTotal' readonly>
      </div>
      <div class="gst" style="font-size:20px;">
        <div class="left-gst">
          <label for="gst" style="display:inline-block;width:150px;">{{$tax2Name}}:</label>
          <label name="gst" id="gst" style="display:inline-block;text-align:right;width:150px;">$ 0.00</label>
        </div>
        <div class="right-gst">
          <label for="svc" style="display:inline-block;width:150px;">{{$tax1Name}}:</label>
          <label name="svc" id="svc" style="display:inline-block;text-align:right;width:150px;">$ 0.00</label>
        </div>
        <input type='hidden' name='gst' class="gst" value='0.00' id='gst' readonly>
        <input type='hidden' name='svc' class="svc" value='0.00' id='svc' readonly>
      </div>
      <div class="total" style="font-size:25px;font-weight: 600;">
        <div class="left-total">
          <label for="total" style="display:inline-block; width:150px;">Total: </label>
          <label name="total" id="total" style="display:inline-block;text-align:right;width:150px;">$ 0.00</label>
        </div>
        <input type='hidden' name='total'  class="total" value='0.00' id='total' readonly>
      </div>
      <button type="button" id="holdbillbtn" class="button w3-round-large" ><h4>Hold Bill</h4></button>
    </div>      
	</form>      
</div>

<!-- Sidebar Navigation -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>
<div class="w3-sidebar w3-collapse w3-animate-left" id="mySidebar">
<div class="sidebar">
  <div class="nav-top w3-show-large">
    <ul class="nav nav-sidebar navTop">
      <li>
        <center><label class="menulbl">Menu</label></center> 
        <hr style="border-top: 1px solid #5d6174;">
      </li>
    </ul>
  </div>
  <div class="nav-top w3-hide-large">
    <ul class="nav nav-sidebar navTop">
      <li>
        <a onclick="w3_close()" title="close menu">
        <img src="{{ asset('images/close.png') }}" style="margin-right:10px;" width="40" height="40"/>
        <label class="menulbl">Menu</label></a> 
        <hr style="border-top: 1px solid #5d6174;">
      </li>
    </ul>
  </div>
  <div class="nav-list">
    <ul name='menu' id='menu' class="navList">
      @foreach($menu as $key=>$m)
        <li><a class='{{$m->dept_nm}}' value="{{$m->dept_cd}}">{{$m->dept_nm}} <br>{{$m->dept_ch}}</a><hr></li>     
      @endforeach
    </ul>
  </div>
  {{-- <div class="nav-footer" id="logout_sidebar_button">
    <ul class="nav nav-sidebar">
      <!-- <button type="button" id="logout" class="w3-bar-item w3-padding logout"><i class="fa fa-sign-out fa-fw w3-margin-right"></i> Log out</button> -->
      <li><a class="logout" id="logout"><i class="fa fa-sign-out fa-fw w3-margin-right"></i> Log out</a></li>
    </ul>
  </div> --}}
</div>
</div>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay Overlay2" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="Overlay2"></div>
<div class="w3-overlay Overlay2" onclick="closeRightMenu()" style="cursor:pointer" id="cartOverlay"></div>


<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:200px;">
  <!-- Header -->
  <div class="top-nav-bar">
    <button class="menuBtn w3-hide-large" onclick="w3_open()"></button>
    <button onclick="openRightMenu()" class="cartBtn"><span class="orders"></span></button>
    <button class="tableBtn" id='selectOutlet'></button>   
  </div>
  <div class="menu-title" id='menutitle'></div>

  <!-- Products -->
  <div class="w3-row-padding" id='showProduct'>
    @foreach($products as $key=>$p)
      {{-- <div class="product-container"> --}}
      <div class="w3-third w3-container w3-margin-bottom">
        {{-- <div class="p-border" id="{{ $p->prod_cd }}"> --}}
        <div class="w3-container w3-white p-border" id="{{ $p->prod_cd }}">
          <div class="image-container">
            @if($p->img_filepath)
            <img src="{{$p->img_filepath}}" alt="" class="prodct-image">
            @else
            <img src="{{ asset('images/noImage.png') }}" alt="" class="prodct-image">
            @endif
          </div>
          <div class="product-content-container">
            <div class='prod_nm'>{{ $p->prod_nm }}</div>
            <div class='prod_ch'>{{ $p->prod_nm_ch }}</div>
            <div class='p-price'>${{$p->price}}</div>
            <div class="product-buttons">
              <div class='quantity'>
                <button type='button' name='menuSub' id='menuSub_{{ $p->prod_cd }}' class='menuSub' data-product-code='{{ $p->prod_cd }}'> - </button>
                <input type='text' name='menu_quantity' size='2' value='1' class='menu_quantity' id='quantity_{{ $p->prod_cd }}' readonly>
                <button type='button' name='menuAdd' id='menuAdd_{{ $p->prod_cd }}' class='menuAdd' data-product-code='{{ $p->prod_cd }}'> + </button>
              </div>
              <button data-product-code="{{ $p->prod_cd }}" data-product-name="{{ $p->prod_nm }}" data-product-price="{{$p->price}}" data-product-chnm="{{$p->prod_nm_ch}}" data-open-price="{{$p->open_price}}" data-tax-1="{{$p->tax_1}}" data-tax-2="{{$p->tax_2}}" name="addtocart" id="addtocart" class="button addToCart hover-addToCart w3-round-large addtocart_{{ $p->prod_cd }}">Add</button>
            </div>
          </div>
        </div>
      </div>
    @endforeach
  </div>
</div>
<!-- End page content -->
<button id="myBtn" class='myBtn' title="Go to top">Top</button>


<!-- Remark Modal -->
  <div class="modal fade" id="remarkModal" tabindex="-1" role="dialog" aria-labelledby="remarkModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="closepax" id="closepax" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="remarkModal" >
          <form id="optionForm" method="POST">
            <h3 id="selectionTitle"></h3>
            <div class="form-group" id="option_selection" data-toggle="buttons"></div>
              <div class="form-group">
                <textarea rows = "3" id="remark" name="remark" width="50%"></textarea>  
              </div>
          </form>
        </div>
       </div>
      <div class="modal-footer remark-footer">
      </div>
    </div>
  </div>
</div>
<!-- End Remark Modal -->

<!-- Open Price Modal -->
<div class="modal fade" id="openPriceModal" tabindex="-1" role="dialog" aria-labelledby="openPriceLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="closepax" id="closepax" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id=""><b>Open Price</b></h4>
      </div>
      <div class="modal-body">
        <div class="openPriceModal">
          <form id="openPriceForm" method="POST">
            <h4><b>Price: </b></h4>
            <div class="form-group">               
              <input type="number" min="0.01" name="openPriceInput" id="openPriceInput">
              <input type='hidden' name='op-name' class="op-name" id='op-name' readonly>
              <input type='hidden' name='op-code' class="op-code" id='op-code' readonly>
              <input type='hidden' name='op-ch-nm' class="op-ch-nm" id='op-ch-nm' readonly>
              <input type='hidden' name='gst' class="gst" id='gst' readonly>
              <input type='hidden' name='svc' class="svc" id='svc' readonly>
            </div>
          </form>
        </div>
       </div>
      <div class="modal-footer">
        <button type='button' name='priceSubmit' id='priceSubmit' class='button addToCart hover-addToCart'>Proceed 继续</button>
      </div>
    </div>
  </div>
</div>
<!-- End Open Price Modal -->

<div class="alert alert-success" id="alertFlashMessage">
    <span></span>
</div>

<div class="alert alert-success" id="alertNotificationFlashMessage">
    <span></span>
</div>


<div class="alertOverlay"></div>
<div class="alert alert-holdbill" id="alertFlashMessageATC">
  <span></span>
</div>

<script>
var APP_URL = {!! json_encode(url('/')) !!};
var taxType = "<?php echo $_SESSION['taxType'];?>";
var tablesession = "<?php  echo $_SESSION['table']; ?>"; 
var gstRate = "<?php echo $_SESSION['gstRate'];?>";
var svcRate = "<?php echo  $_SESSION['svcRate'];?>";
var tableOccupied = "<?php echo $_SESSION['tableOccupied'];?>";
var tableOccupiedRcpNo = "<?php echo $_SESSION['tableOccupiedRcpNo'];?>";
var tableNameOccupied = "<?php echo $_SESSION['tableNameOccupied'];?>";
var tableName = "<?php echo $_SESSION['tablename'];?>";
var itemquantity;


$( document ).ready(function() {
  if(tableOccupied != ''){
    sessionOccupied();
  }
});

subtotal();

function preventBack() { 
  window.history.forward(); 
}
setTimeout("preventBack()", 0);
window.onunload = function () { null };


$(document).on('click', 'button#voidBtn', function () {
  var prod_cd = $(this).data('product-code');
  var linkCD = $(this).data('linkcd');
  var trid = $(this).data('trid');
  var quantity = $(this).closest('tr').find("input[name='quantity[]']").val();
  voidItem(prod_cd,tableOccupiedRcpNo,linkCD,trid,quantity);
});


$( document ).ready(function() {
  $(window).scroll(function() {
      if ($(this).scrollTop() >= 20) {
          $('#myBtn').fadeIn(200);
      } else {
          $('#myBtn').fadeOut(200);
      }
  });
});
$('#myBtn').click(function() {
    $('body,html').animate({
        scrollTop : 0
    }, 500);
});

$(function() {
    var menuname = $('#menu li').first().text();
    var menu = $("ul.navList li a").first().attr('value');
    $('#menutitle').append("<h1><b>"+menuname+"</b></h1>");
    getNewProducts(menu,menuname);
});


// Script to open and close sidebar
function w3_open() {
    document.getElementById("mySidebar").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}
 
function w3_close() {
    document.getElementById("mySidebar").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}

function openRightMenu() {
  document.getElementById("rightMenu").style.display = "block";
  document.getElementById("cartOverlay").style.display = "block";
}

function closeRightMenu() {
  document.getElementById("rightMenu").style.display = "none";
  document.getElementById("cartOverlay").style.display = "none";
}

function subtotal(){
  if(taxType == 'E'){
    document.getElementsByClassName("cart-cal")[0].style.height = "150px";
    document.getElementsByClassName("fixed-panel")[0].style.marginBottom = "150px";
    document.getElementsByClassName("subTotal")[0].style.display = "block";
  }else{
    document.getElementsByClassName("cart-cal")[0].style.height = "120px";
    document.getElementsByClassName("fixed-panel")[0].style.marginBottom = "120px";
    document.getElementsByClassName("subTotal")[0].style.display = "none";
  }
}


$(function() {
  $("ul.navList li a").on("click",function() {
    var menuname = $(this).text();
    var menu =  $(this).attr('value');
   getNewProducts(menu,menuname);
  });
});

$(document).on('click', '#addtocart', function(){    
  // get input values
  var prod_name = $(this).data('product-name');
  var prod_price = $(this).data('product-price');
  var prod_code = $(this).data('product-code');
  var prod_ch_nm = $(this).data('product-chnm');
  var open_price = $(this).data('open-price');
  var gst = $(this).data('tax-2');
  var svc = $(this).data('tax-1');

  if(open_price == 1 && prod_price == 0){
    $('#op-name').val(prod_name);
    $('#op-code').val(prod_code);
    $('#op-ch-nm').val(prod_ch_nm);
    $('#gst').val(gst);
    $('#svc').val(svc);
    $('#openPriceInput').val("");
    $('#openPriceModal').modal('toggle');
  }else
    addToCart(prod_name,prod_price,prod_code,prod_ch_nm,svc,gst);
});

$(document).on('click', '#priceSubmit', function(){  
  var prod_name = $('#op-name').val();
  var prod_price = $('#openPriceInput').val();
  var prod_code = $('#op-code').val();
  var prod_ch_nm = $('#op-ch-nm').val();
  var gst = $('#gst').val();
  var svc = $('#svc').val();
  if(prod_price <= 0){
    alert('Please enter a price.');
  }else{
    addToCart(prod_name,prod_price,prod_code,prod_ch_nm,svc,gst);
    $("#openPriceModal").modal('hide');
  }
});

function makeid(length) {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

function addToCart(prod_name,prod_price,prod_code,prod_ch_nm,svc,gst){
  var i = makeid(3);
  if(prod_name.length>45){
    var prod_name1 = prod_name.substring(0,43)+"...";
  }else{
    var prod_name1 = prod_name;
  }
  if(prod_ch_nm.length>20){
    var prod_ch_nm1 = prod_ch_nm.substring(0,19)+"...";
  }else{
    var prod_ch_nm1 = prod_ch_nm;
  }

  var table = document.getElementsByTagName('table')[0];
  // checkSelection(prod_code);
  var quantity = $('#quantity_'+prod_code).val();
  var newRow = table.insertRow(table.rows.length);
  newRow.id= prod_code+'_'+i;

  // add cells to the row
  var cel1 = newRow.insertCell(0);
  
  html = "<input type='hidden' name='remark_code[]' value='"+i+"' readonly>";
  html += "<div class='cart-title' style='display:block; width:100%'><div class='ct-box1'><input type='hidden' class='prod_code' name='product_code[]' value='"+prod_code+"' id='code_"+prod_code+"' data-gst='"+gst+"' data-svc='"+svc+"' readonly>";
  html += "<input type='hidden' name='product_name[]' value='"+prod_name+"' id='"+prod_name+"' readonly>"+prod_name1+ '<br>'+prod_ch_nm1;
  html += "<div id='remark_"+prod_code+"_"+i+"' style='padding-left:10px; font-size:15px;'></div></div>";
  html += "<div class='ct-box2'><input type='hidden' name='product_price[]' class='val1' value='"+prod_price+"' id='"+prod_price+"' readonly>"+prod_price+"</div></div>";
  html += "<div class='cart-btn'><div class='cb-box1'><button type='button' name='option' id='option' class='option' data-remark-code='"+i+"' data-product-code='"+prod_code+"'> Remark </button></div>";
  html += "<div class='cb-box2'><button type='button' name='sub' id='sub' class='sub' data-product-code='"+prod_code+"'> - </button>";
  html += "<input type='text' name='quantity[]' size='2' value='"+quantity+"' class='val2' id='count_"+prod_name+"' readonly>";
  html += "<button type='button' name='add' id='add' class='add' data-product-code='"+prod_code+"'> + </button></div>";
  html += "<div class='cb-box3'><input type='hidden' name='multTotal[]' class='multTotal_"+prod_code+"' value='0' readonly><button type='button' class='deletebtn' data-product-code="+prod_code+" data-product-name="+prod_name+" title='Remove item'></button></div></div>";
  cel1.innerHTML = html;
  if(itemquantity){
    itemquantity += parseInt(quantity);
  }else{
    itemquantity = 0;
    itemquantity+= parseInt(quantity);
  }
  if(itemquantity > 0){
      $(".cartBtn span").text(itemquantity);
      $('.orders').css({'display':'block'});
    }
  $('#quantity_'+prod_code).val('1');
  $('.fixed-panel').css({'display':'block'});
  var element = document.getElementById(''+prod_code+'_'+i);
  element.classList.add("txtMult");
  alertDialog('Item added to cart.');
  multInputs();
}

/* $(document).on('click', '#proceed', function(){
	var numberOfPax = $('#numberOfPax').val();
	holdBill();		   
}); */

$(document).on('click', '#closepax', function(){  
  $('.overlay').css({'display':'none'});	
});

function multInputs() {
  var total = 0;
  var gstTotal = 0;
  var svcTotal = 0;
  var subTotal = 0;
  var grandTotal = 0;

  // for each row:
  $("tr.txtMult").each(function () {
    // get the values from this row:
    var prod_code = $('.prod_code', this).val();
    var price = $('.val1', this).val();
    var quantity = $('.val2', this).val();
    var svc = $("#code_"+prod_code, this).data('svc'); 
    var gst = $("#code_"+prod_code, this).data('gst');       
    var grossPrice = price * quantity;
    var net = 0;
    var svcCharge = 0;
    var gstCharge = 0;
    $('input[type=hidden].multTotal_'+prod_code).val(grossPrice);

    if(taxType == 'E'){
      if(svc == 1){
        svcCharge = grossPrice * svcRate;
        svcTotal += svcCharge;
      }
      if(gst == 1){
        var w_svc = grossPrice + svcCharge;
        gstCharge = w_svc * gstRate;
        gstTotal += gstCharge;
      }
      
      subTotal += grossPrice;
      net = grossPrice + svcCharge + gstCharge;
      total += net;
    }else if(taxType == 'I'){     
      if(gst == 1){        
        gstCharge = grossPrice / gstRate;
        var gstAmount = grossPrice - gstCharge;
        gstTotal += gstAmount;
      }
      if(svc == 1){
        if(gst == 1){
          svcCharge = gstCharge / svcRate;
          var svcAmount = gstCharge - svcCharge;
          svcTotal += svcAmount;
        }else{
          svcCharge = grossPrice / svcRate;
          var svcAmount = grossPrice - svcCharge;
          svcTotal += svcAmount;
        }
      }

      total += grossPrice;
    }   
  });

  svc = parseFloat(svcTotal).toFixed(2);
  gst = parseFloat(gstTotal).toFixed(2);
  grandTotal = parseFloat(total).toFixed(2);

  if(taxType == 'E'){
    subTotal = parseFloat(subTotal).toFixed(2);
    $("#subTotal").text('$ '+subTotal);
    $('input[type=hidden].subTotal').val(subTotal);
  }
  $("#gst").text('$ '+gst);
  $("#svc").text('$ '+svc);  
  $("#total").text('$ '+grandTotal);
  $('input[type=hidden].gst').val(gst);
  $('input[type=hidden].svc').val(svc);
  $('input[type=hidden].total').val(grandTotal);
}

//Cart Buttons
$(document).on('click', 'button.deletebtn', function () {
  var prod_code = $(this).data('product-code');
  var prod_name = $(this).data('product-name');
  var quantity = $(this).closest('tr').find("input[name='quantity[]']").val();
  itemquantity -= quantity;
  $(this).closest('tr').remove();
	// $(".addtocart_"+prod_code).prop("disabled", false);
  // $("#menuSub_"+prod_code).prop("disabled", false);
  // $("#menuAdd_"+prod_code).prop("disabled", false);
  // $("input[type=text]#quantity_"+prod_code).val('1');
  // $("input[type=text]#quantity_"+prod_code).prop("disabled", false);
  
  if(itemquantity > 0){
    $(".cartBtn span").text(itemquantity);
    $('.orders').css({'display':'block'});
  }else{
    $('.orders').css({'display':'none'});
  }
  var tbody = $("#cartTbl tbody");
  if(tbody.children().length == 0){
    $('.fixed-panel').css({'display':'none'});
  }
	multInputs();
});

$(document).on('click', 'button.menuAdd', function () {
  if ($(this).prev().val() < 99) {
    $(this).prev().val(+$(this).prev().val() + 1);
  }
});

$(document).on('click', 'button.menuSub', function () {
  if ($(this).next().val() > 1) {
    if ($(this).next().val() > 1) 
      $(this).next().val(+$(this).next().val() - 1);
  }
});

$(document).on('click', 'button.add', function () {
  if ($(this).prev().val() < 99) {
    $(this).prev().val(+$(this).prev().val() + 1);
    itemquantity += 1;
    $(".cartBtn span").text(itemquantity);
  }
  multInputs();
});

$(document).on('click', 'button.sub', function () {
  if ($(this).next().val() > 1) {
    if ($(this).next().val() > 1){ 
      $(this).next().val(+$(this).next().val() - 1);
      itemquantity -= 1;
      if(itemquantity > 0){
        $(".cartBtn span").text(itemquantity);
      }
    }
  }
  multInputs();
});

$(document).on('click', 'button.option', function () {
  document.getElementById("loader").style.display = "block";
  var prod_code = $(this).data('product-code');
  var remark_code = $(this).data('remark-code');
  $.ajax({
      type: "GET",
      url: APP_URL+"/api/v1/getSelection",
      data: $.param({'prod_code':prod_code}),
      success: function(data){
        if(data.error == false){
          var data = data['data'];
          if(data.length > 0){
            html = "";
            $.each(data, function( index, value ) {                
              html += '<label class="btn btn-primary"><input type="checkbox" name="optionCheckbox" autocomplete="off" name="option_'+prod_code+'_'+value.KitchenRequestCode+'" id="option_'+prod_code+'_'+value.KitchenRequestCode+'"  value="'+value.KitchenRequestDescription+'">'+value.KitchenRequestDescription+'</label>';
              if($('#option_'+prod_code+'_'+value.KitchenRequestCode).length !==0){
                $('#option_'+prod_code+'_'+value.KitchenRequestCode).prop('checked',true);
              }
            });
            document.getElementById('option_selection').innerHTML = html; 
            $('#selectionTitle').text('Remark');
            button = "<button type='button' name='option_submit' id='option_submit' data-p-code='"+prod_code+"' data-remark-code='"+remark_code+"' class='button green hover-green'>Continue</button>";
            $('.remark-footer').html(button);
            $('textarea').val('');
            $('#remarkModal').modal('toggle');
          }else{
            document.getElementById('option_selection').innerHTML = " "; 
            $('#selectionTitle').text('No remark option available for this item.');
            button = "<button type='button' name='option_submit' id='option_submit' data-p-code='"+prod_code+"' data-remark-code='"+remark_code+"' class='button green hover-green'>Continue</button>";   
            $('.remark-footer').html(button);
            $('textarea').val('');
            $('#remarkModal').modal('toggle');
          }
        }else{
          alert('Fail to retrieve kitchen request.');
        }  
        document.getElementById("loader").style.display = "none"; 
      },error: function(){
        document.getElementById("loader").style.display = "none";
        alert('Problem occur. Please try again');
      }
  }); 	
});


$('#holdbillbtn').click(function(event){
    holdBill();		
});

function holdBill(){
  document.getElementById("loader").style.display = "block";
  $.ajax({
    type: "POST",
    url: APP_URL+"/api/v1/holdBill",
    data: $('#checkoutForm').serialize(),
    success: function(data){
      if(data.error == false){
        if(data.code == '200'){
          alertDialogATC(data.message);
          window.setTimeout(function(){
            window.onbeforeunload = null;
            window.location.href = APP_URL+"/tableselection"; 
          }, 1000);
        }else if(data.code == '400'){
          alert(data.message);  
        }
      }else{
        alert(data.message);
      }
      document.getElementById("loader").style.display = "none";
    },error: function(){
      $('#holdbillbtn').prop('disabled',false);
      document.getElementById("loader").style.display = "none";
      alert('Problem occur. Please try again');
    },
    statusCode: {
      300: function() {
        alert(data.message);
        window.onbeforeunload = null;
        window.location.href = APP_URL+"/login";
      }
    }
  });
}

/* function checkSelection(prod_code){
  document.getElementById("loader").style.display = "block";
  $.ajax({
    type: "GET",
    url: APP_URL+"/api/v1/getSelection",
    data: $.param({'prod_code':prod_code}),
    success: function(data){
      if(data.error == false){
          var returnedData = data;
          var data = data['data'];
          if(data.length>0){
            $(".addtocart_"+prod_code).prop("disabled", false);
            $("#menuSub_"+prod_code).prop("disabled", false);
            $("#menuAdd_"+prod_code).prop("disabled", false);
            $("input[type=text]#quantity_"+prod_code).val('1');
            $("input[type=text]#quantity_"+prod_code).prop("disabled", false);
          }else
          {
            $(".addtocart_"+prod_code).prop("disabled", true);
            $("#menuSub_"+prod_code).prop("disabled", true);
            $("#menuAdd_"+prod_code).prop("disabled", true);
            $("input[type=text]#quantity_"+prod_code).val('1');
            $("input[type=text]#quantity_"+prod_code).prop("disabled", true);
          }
      }else{
          $(".addtocart_"+prod_code).prop("disabled", false);
          $("#menuSub_"+prod_code).prop("disabled", false);
          $("#menuAdd_"+prod_code).prop("disabled", false);
          $("input[type=text]#quantity_"+prod_code).prop("disabled", false);
      }
      document.getElementById("loader").style.display = "none";
    },error: function(){
      document.getElementById("loader").style.display = "none";
      alert('Problem occur. Please try again');
    }
  });
} */

$(document).on('click', '#option_submit', function () {
  var prod_code = $(this).data('p-code');
  var remark_code = $(this).data('remark-code');
  var data = $('#optionForm').serializeArray();
  html = "";
  $("#remark_"+prod_code+'_'+remark_code).empty();
  jQuery.each( data, function( i, field ) {
    if(field.value != ''){
       html += "<input type='hidden' name='"+prod_code+"_"+remark_code+"_product_remark[]' value='"+field.value+"' readonly>";
       html += "<div id="+field.name+" style='padding-left:10px;'>*"+field.value+"<br></div>";
     }
    }); 
    $('#remark_'+prod_code+'_'+remark_code).append(html);
    $('#remarkModal').modal('toggle');
});
//Cart Buttons End

//Nav Functions
$('#selectOutlet').click(function(event){
  document.getElementById("loader").style.display = "block";
  var	tableUrl = APP_URL+'/tableselection';
  checkAndDeleteUsingTable(tableUrl);
});

function getNewProducts(menu,menuname){
  document.getElementById("loader").style.display = "block";
	$('.Overlay2').css({'display':'block'});
	var html="";
  $.ajax({
    type: "GET",
    url: APP_URL+"/api/v1/getNewProducts",
    data: $.param({'menu': menu}),
    success: function(data){	  
      var data = data['data'];
      if(data.length > 0){
        $.each(data, function( index, value ) {				
          $("#showProduct").empty();
          $("#menutitle").empty();
          var ori_nm_ch = value.prod_nm_ch;
          if(ori_nm_ch.length>12){
            var nm_ch = ori_nm_ch.substring(0,11)+"...";
          }else{
            var nm_ch = ori_nm_ch;
          }

          if(value.img_filepath){
            var imagePath = value.img_filepath;
          }else{
            var imagePath = "{{ asset('images/noImage.png') }}";
          }

          // html +="<div class='product-container'>";
          html +='<div class="w3-third w3-container w3-margin-bottom">';
          // html +="<div class='p-border' id='"+value.prod_cd+"'>";
          html +="<div class='w3-container w3-white p-border' id='"+value.prod_cd+"'>"
          html +="<div class='image-container'><img src='"+imagePath+"' class='prodct-image'></div>";      
          html +="<div class='product-content-container'>";
          html +="<div class='prod_nm'>"+value.prod_sh_nm+"</div>";
          html +="<div class='prod_ch'>"+nm_ch+"</div>";
          html +="<div class='p-price'>$"+value.price+"</div>";
          html +="<div class='product-buttons'><div class='quantity'><button type='button' name='menuSub' id='menuSub_"+value.prod_cd+"' class='menuSub' data-product-code='"+value.prod_cd+"'> - </button>";
          html +="<input type='text' name='menu_quantity' size='2' value='1' class='menu_quantity' id='quantity_"+value.prod_cd+"' readonly>";
          html +="<button type='button' name='menuAdd' id='menuAdd_"+value.prod_cd+"' class='menuAdd' data-product-code='"+value.prod_cd+"'> + </button></div>";
          html +="<button data-product-code='"+value.prod_cd+"' data-product-name='"+value.prod_sh_nm+"' data-product-price='"+value.price+"' data-product-chnm='"+value.prod_nm_ch+"' data-open-price='"+value.open_price+"' data-tax-1='"+value.tax_1+"' data-tax-2='"+value.tax_2+"' name='addtocart' id='addtocart' style='margin-top:5px;' class='button addToCart hover-addToCart w3-round-large addtocart_"+value.prod_cd+"' >Add</button><br/><br/></div></div></div></div>";
        });
        $('#showProduct').append(html);
        $('#menutitle').append("<h1><b>"+menuname+"</b></h1>");
        w3_close();
      }else{
        alert('No product in this menu.');
           
      }
      $('.Overlay2').css({'display':'none'});
      document.getElementById("loader").style.display = "none";
    },error: function(){
      $('.Overlay2').css({'display':'none'});
      document.getElementById("loader").style.display = "none";
      alert('Problem occur. Please try again ');
    }
  });
}
//End Nav function

$('.Overlay2').click(function(event){
  $('.Overlay2').css({'display':'none'});	
});
/* 

//Refresh page
var time = new Date().getTime();
$(document.body).bind("mousemove keypress", function(e) {
    time = new Date().getTime();
});

function refresh() {
    if(new Date().getTime() - time >= 300000) 
        window.location.reload(true);
    else 
        setTimeout(refresh, 100000);
}

setTimeout(refresh, 100000);
//End of Refresh Page */

function alertDialog(string){
    $('#alertFlashMessage span').text(string);
    $('#alertFlashMessage').fadeIn('normal', function() {
      $(this).delay(1200).fadeOut();
   });
}

function alertNotification(string){
    $('#alertNotificationFlashMessage span').text(string);
    $('#alertNotificationFlashMessage').fadeIn('normal', function() {
      $(this).delay(1500).fadeOut();
   });
}

function alertDialogATC(string){
  $('.alertOverlay').fadeIn('normal', function() {
      $(this).delay(1000).fadeOut();
   });
 
  $('#alertFlashMessageATC span').text(string);
  $('#alertFlashMessageATC').fadeIn('normal', function() {
    $(this).delay(1000).fadeOut();
  });
}

function sessionOccupied(){
    if(tableOccupied != ''){
      document.getElementById("loader").style.display = "block";
          $('.Overlay2').css({'display':'block'});
          $.ajax({
            type: "GET",
            url: APP_URL+"/api/v1/getOccupiedItem",
            data: $.param({'table': tableOccupied,'tableNameOccupied':tableNameOccupied}),
            success: function(data){	  
              var details = data['details'];
              var headers = data['headers'];
              if(details && details.length > 0){
                itemquantity = 0;
                $.each(details, function( index, value ) {
                  if(value.PROD_NM.length>45){
                    var prod_name1 = value.PROD_NM.substring(0,43)+"...";
                  }else{
                    var prod_name1 = value.PROD_NM;
                  }

                  if(value.PROD_NM_CH.length>20){
                    var prod_ch_nm1 = value.PROD_NM_CH.substring(0,19)+"...";
                  }else{
                    var prod_ch_nm1 = value.PROD_NM_CH;
                  }

                  var table = document.getElementsByTagName('table')[0];
                  var i = makeid(3);
                  var newRow = table.insertRow(table.rows.length);
                  newRow.id = value.PROD_CD+'_'+i;
                  var cel1 = newRow.insertCell(0);
                  var price = parseFloat(value.SA_PRICE).toFixed(2);
                  var remarks = value.Remarks;
                  
                  html = "<input type='hidden' name='remark_code[]' value='"+i+"' readonly>";
                  html += "<div class='cart-title' style='display:block; width:100%'><div class='ct-box1'><input type='hidden' class='prod_code' name='product_code[]' value='"+value.PROD_CD+"' id='code_"+value.PROD_CD+"' data-gst='"+value.gst+"' data-svc='"+value.svc+"'  readonly>";
                  html += "<input type='hidden' name='linkCD[]' value='"+value.link_cd+"'>"
                  html += "<input type='hidden' name='product_name[]' value='"+value.PROD_NM+"' id='"+value.PROD_NM+"'  readonly>"+prod_name1+ '<br>'+prod_ch_nm1;
                  html += "<div id='remark_"+value.PROD_CD+"_"+i+"' style='padding-left:10px; font-size:15px;'>";
                  if(remarks != ' '){
                    $.each(remarks, function( index1, value1 ) {
                      if(value1 !=null){
                        html += "<input type='hidden' name='"+value.PROD_CD+"_"+value1+"_product_remark[]' value='"+index1+"' readonly>";
                        html += "<div id="+value.PROD_NM+" style='padding-left:10px;'>*"+index1+"<br></div>";
                      }
                    })
                  }
                  html += "</div></div>";
                  html += "<div class='ct-box2'><input type='hidden' name='product_price[]' class='val1' value='"+price+"' id='"+price+"'  readonly>"+price+"</div></div>";
                  html += "<div class='cart-btn'><div class='cb-box1'><button type='button' name='option' id='option' class='option' data-remark-code='"+i+"' data-product-code='"+value.PROD_CD+"' disabled> Remark </button></div>";
                  html += "<div class='cb-box2'><button type='button' name='sub' id='sub' class='sub' data-product-code='"+value.PROD_CD+"' disabled> - </button>";
                  html += "<input type='text' name='quantity[]' size='2' value='"+value.SA_QTY+"' class='val2' id='count_"+value.PROD_NM+"'  readonly>";
                  html += "<button type='button' name='add' id='add' class='add' data-product-code='"+value.PROD_CD+"' disabled> + </button></div>";
                  html += "<div class='cb-box3'><input type='hidden' name='multTotal[]' class='multTotal_"+value.PROD_CD+"' value='0'  readonly><button type='button' class='voidBtn' id='voidBtn' data-product-code="+value.PROD_CD+" data-linkCD="+value.link_cd+" data-trId="+i+" title='Remove item'>X</button></div></div>";
                  cel1.innerHTML = html;
                  var quantity = value.SA_QTY;
                  itemquantity += parseInt(quantity);
                  if(itemquantity > 0){
                    $(".cartBtn span").text(itemquantity);
                    $('.orders').css({'display':'block'});
                  }
                  $('.fixed-panel').css({'display':'block'});
                  var element = document.getElementById(''+value.PROD_CD+'_'+i);
                  element.classList.add("txtMult");
                  multInputs();
                })
            }else{
              alert('No product in this menu.'); 
            }
           
            $('.Overlay2').css({'display':'none'});
            document.getElementById("loader").style.display = "none";
          },error: function(){
             $('.Overlay2').css({'display':'none'});
            document.getElementById("loader").style.display = "none";
            alert('Problem occur. Please try again ');
          }
        });
    }
}

function voidItem(prod_code,tableOccupiedRcpNo,linkCD,trid,quantity){
  document.getElementById("loader").style.display = "block";
  $.ajax({
    type: "POST",
    url: APP_URL+"/api/v1/voidUpdateItem",
    data: $.param({'order':prod_code,'rcpNo':tableOccupiedRcpNo,'linkCD':linkCD}),
    success: function(data){
      if(data.error == false){
        // $(".addtocart_"+prod_code).prop("disabled", false);
        // $("#menuSub_"+prod_code).prop("disabled", false);
        // $("#menuAdd_"+prod_code).prop("disabled", false);
        // $("input[type=text]#quantity_"+prod_code).val('1');
        // $("input[type=text]#quantity_"+prod_code).prop("disabled", false);
        itemquantity -= quantity;
        if(itemquantity > 0){
          $(".cartBtn span").text(itemquantity);
          $('.orders').css({'display':'block'});
        }
        var tbody = $("#cartTbl tbody");
        if(tbody.children().length == 0){
          $('.fixed-panel').css({'display':'none'});
        }
	      multInputs();
        $('table#cartTbl tr#'+prod_code+'_'+trid).remove();
        alertDialog('Item Voided.');
      }else{
        alert(data.message);
      }
      document.getElementById("loader").style.display = "none";
    },error: function(){
      document.getElementById("loader").style.display = "none";
      alert('Problem occur. Please try again');
    }
  });
}

function checkAndDeleteUsingTable(url){
  document.getElementById("loader").style.display = "block";
  $.ajax({
    type: "POST",
    url: APP_URL+"/api/v1/checkAndDeleteUsingTable",
    success: function(data){
      if(data.error == false){ 
        window.onbeforeunload = null;         
        window.location.href = url;
      }else{
        alert(data.message);
      }
      document.getElementById("loader").style.display = "none";
    },error: function(){
      document.getElementById("loader").style.display = "none";
      window.onbeforeunload = null;  
      window.location.href = url;
    }
  });
}

window.setInterval(function(){
  $.ajax({
    type: "GET",
    url: APP_URL+"/api/v1/getOrderReview",
    success: function(data){
        if(data.error == false){
          var ready = data['ready'];
          if(ready && ready.length > 0){
            alertNotification('The food is ready to serve.');
          }
        }
    },error: function(){
    }
  });
}, 60000);

window.onbeforeunload = function(event) {
  event.returnValue = "Write something clever here..";
};

window.addEventListener('unload', (event) => {
    navigator.sendBeacon(APP_URL+"/api/v1/checkAndDeleteUsingTable");
});

</script>

</body>
</html>
