<head>
  <meta charset="UTF-8">
  <title>Cafe</title>
  <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no" />
  <link href="{!! asset('images/favicon.ico') !!}" rel="shortcut icon" type="image/x-icon" />
  <meta name="apple-mobile-web-app-capable" content="yes">
  <link rel="apple-touch-icon" sizes="128x128" href="{!! asset('images/favicon.ico') !!}">
  
  <!-- Bootstrap 3.3.2 -->
  <link href="{{ asset('AdminLTE/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  
  <!-- Font Awesome Icons -->
  <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
  
  <!-- Ionicons -->
  <link href="{{ asset('css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('AdminLTE/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />

  <!--js-->
  <script src="{{ asset('js/jquery.min.js') }}"></script>

  <!-- jquery ui -->
  <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
  <script src="{{ asset('js/jquery-ui.js') }}"></script>

  <!-- jquery validate plugin -->
  <script src="{{ asset('js/jquery.validate.js') }}"></script>
  
  <script language="javascript" type="text/javascript" src="{{ asset('js/sha1.js') }}"></script>
  <style>
    @font-face {
      font-family: 'Avenir';
      src: url('/EASIWebOrder_SATS/public/fonts/Avenir_95_Black.ttf'),
          url('/EASIWebOrder_SATS/public/fonts/Avenir-Black-webfont.woff'),
          url('/EASIWebOrder_SATS/public/fonts/avenir-black.woff2'),
          url('/EASIWebOrder_SATS/public/fonts/Avenir-Black-webfont.eot'),
          url('/EASIWebOrder_SATS/public/fonts/Avenir-Black.svg');
    }

    body{
      font-family:'Avenir';
    }
    #customBtn:hover {
      cursor: pointer;
    }

    .loginBox{
      margin: 0;
      position: absolute;
      top: 50%;
      left: 50%;
      -ms-transform: translate(-50%, -50%);
      transform: translate(-50%, -50%);  
      width:40%;    
    }

    .inputBox{
      -webkit-appearance: none;
      width:100%;
      height: 50px;
      padding: 6px 25px;
      box-shadow: 0 5px 10px rgba(0, 0, 0, 0.4);
      border-radius: 60px;
      background-color: #ffffff;
      font-size: 15px;
      border: none;
    }

    .submitBtn{
      height: 50px;
      width: inherit;
      color: white;
      box-shadow: 0 5px 10px rgba(93, 97, 116, 0.27);
      border-radius: 60px;
      background-image: linear-gradient(180deg, #777b90 0%, #5d6174 100%);
      text-transform: uppercase;
      transition: 0.2s ease;
      cursor: pointer;
      margin: auto;
      font-size: 18px;
      border: none;
      margin-top:30px;
    }

    .submitBtn:hover,
    .submitBtn:focus{
      background-image: linear-gradient(180deg, #545457 0%, #5d6174 100%);
      transition: 0.2s ease;
    }

    .input_icon{
      position: absolute;
      right:20px;
      z-index: 2;
      display: block;
      line-height: 50px;
      text-align: center;
      pointer-events: none;
    }

    .padding-bottom{
      padding-bottom:20px;
    }
    
  </style>
</head>
<body>
  <div>
    <section class="content">
      <div class="loginBox">
        <div class="login-logo">
          <img src="{{ asset('images/logo.png') }}"/>
        </div>
        <div class="loginBody">
          <div>
            @if ($errors->has())
              <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                  {{ $error }}<br>        
                @endforeach
              </div>
            @elseif(Session::has('success'))
              <div class="alert alert-success" id="success-alert">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                {{ Session::get('success') }}
              </div>
            @elseif(Session::has('fail'))
              <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                {{ Session::get('fail') }}
              </div>
            @elseif (Session::has('message'))
              <div class='bg-danger alert'>{{ Session::get('message') }}</div>
            @endif            
          </div>
        
          <form method="post" id="loginForm" action="{{ asset('login') }}">
            <div class="form-group has-feedback padding-bottom">	
              <span class="glyphicon glyphicon-user input_icon"></span>
              <input type="text" name="usercode" placeholder="Username" class="inputBox" value="{{ old('usercode') }}"><br>					
            </div>
            <div class="form-group has-feedback padding-bottom">
              <span class="glyphicon glyphicon-lock input_icon"></span>
              <input name="password" placeholder="Password" type="password" class="inputBox">
            </div>
              <input type="hidden" name="keyPhrase" value="3mP">
            <div class="row">
              <div class="col-xs-4 pull-right text-right" style="width: 100%;text-align: center;">
                <button type="submit" class="submitbtn">Sign In</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
  </div>   
</body>
<script>
var APP_URL = {!! json_encode(url('/')) !!};

$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
    $("#success-alert").slideUp(500);
});
</script>
</html>