<?php
function md5hash($keyPhrase){
    $key = mb_convert_encoding($keyPhrase, "ASCII");
    $key = md5($key,true);
    $key .= substr($key, 0, 8);
    return $key;
}

function ConnectDb($dbinfo){
    config(['database.connections.KPT' => [
        'driver' => 'sqlsrv',
        'host' => $dbinfo['host'],
        'database' => $dbinfo['dbName'],
        'username' => $dbinfo['dbUserName'],
        'password' => $dbinfo['dbPassword']
    ]]);

    return DB::connection('KPT');
}

function decryptDbInfo($encrypted_string = null){
    $key = md5hash('EASIUtilities');
    $iv = '';

    $decrypted_string = openssl_decrypt($encrypted_string, 'des-ede3', $key, 0, $iv);

    return $decrypted_string;
}

$apiKey = Session::get('apiKey');
$userCode = DB::table('apiauth')->where('apikey',$apiKey)->value('userid');
$userTable = DB::connection('sqlsrv2')->table('tablesmapset')->where('tblEmpid',$userCode)->get();
$allOrder = 0;
foreach($userTable as $u){
    $rcpNo = DB::connection('sqlsrv2')->table('tablesmapset')->where('tblsysid',$u->tblSysID)->where('tblnamedefined',$u->tblNameDefined)->value('tblrcpno');
    $deptCd = DB::connection('sqlsrv2')->table('sysflag')->where('flag','SK9')->value('Setting');
    $detailsDptCd = DB::connection('sqlsrv2')->table('details')->where('rcp_no',$rcpNo)->whereNotNull('kpt_no')->select($deptCd)->distinct()->get();
    $orderNo = DB::connection('sqlsrv2')->table('details')->where('rcp_no',$rcpNo)->select('ord_No')->distinct()->get();			
    
    if($detailsDptCd){
        foreach($detailsDptCd as $d){

            $kpt='[KPT'.$d->$deptCd.']';
                
            $lines = file('EASICRYPT/EASI.ini', FILE_IGNORE_NEW_LINES);
            
            $result = array();
            $index = -1;
            foreach ($lines as $l) {
                if ($l == $kpt) {
                    $index++;
                }
                $result[$index][] = $l;
            }

            $server = decryptDbInfo(str_replace("1=","",$result[0][1]));
            $dbName = decryptDbInfo(str_replace("2=","",$result[0][2]));
            $dbUserName = decryptDbInfo(str_replace("3=","",$result[0][3]));
            $dbPassword = decryptDbInfo(str_replace("4=","",$result[0][4]));
            $windowAuthenticate = decryptDbInfo(str_replace("5=","",$result[0][5]));
            $crypt = decryptDbInfo(str_replace("6=","",$result[0][6]));

            $dbinfo = array('host'=>$server,'dbName'=>$dbName,'dbUserName'=>$dbUserName,'dbPassword'=>$dbPassword);
            
            $dbConnection = ConnectDb($dbinfo);
            
            foreach($orderNo as $o){
                $orders = $dbConnection->table('order_table')->where('rcp_no',$rcpNo)->where('order_number',$o->ord_No)->whereNotNull('prepare_dt')->whereNull('collect_dt')->count();
                $allOrder += $orders; 
            }	
            DB::purge('KPT');						
        } 
                                    
    }
}

echo $allOrder;
?>

