<!DOCTYPE html>
<html>
<head>
  <title>Web Order</title>
  <link href="<?php echo asset('images/favicon.ico'); ?>" rel="shortcut icon" type="image/x-icon" />
  <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no" />
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <link href="<?php echo e(asset('AdminLTE/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
  <script src="<?php echo e(asset('js/bootstrap.js')); ?>"></script>
  <!-- css -->
  <link rel="stylesheet" href="<?php echo e(asset('css/style.css')); ?>">
  <style>
  /*General CSS */
    html{
      overscroll-behavior: contain;
    }

    h2{
      margin: 15px;
    }

    select {
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;
      width: 300px;
      box-shadow: 0 3px 6px rgba(0, 0, 0, 0.5);
      border-radius: 33px;
      border: none;
      padding: 10px 30px;
      font-size: large;
      background-color: #fff;
    }

    .select-container {position:relative; display: inline;}
    .select-container:after {content:""; width:0; height:0; position:absolute; pointer-events: none;}
    .select-container:after {
        border-left: 5px solid transparent;
        border-right: 5px solid transparent;
        top: .3em;
        right: .75em;
        border-top: 8px solid black;
        opacity: 0.5;
    }

    select::-ms-expand {
        display: none;
    }

    #outlet{
      height:100%;
    }

    .selectionContainer{
      box-shadow: 0 5px 9px rgba(0, 0, 0, 0.5);
      border-radius: 35px;
      background-color: #ffffff;
      text-align:center;
      width:80%;
      margin:auto;
      padding: 10px;
    }

    .selectFloorDiv{
      margin-bottom:35px;

    }

    .tableSelectionContainer {
      align-items: center;
    }

    .buttons{
      text-align:center;
    }

    .NotificationModal,.tableModal{
      text-align: center;
    }

    #closeModalBtn{
      background: url('<?php echo e(asset('images/close.png')); ?>') no-repeat;
      float:right;
      background-size: 90%; 
      width: 40px;
      height: 40px;
      border: none;
      background-color: transparent;
      /* float: right;
      display: inline-block;
      font-size: 30px;
      width: 55px;
      cursor:pointer; 
      border: none;   */  
    }

    /* .p1{
      padding: 0px;
      margin: 0 0 8px;
      background-color: white;
      border: none;
      border-radius: 0px;
      font-family: monospace;
    }

    .p2{
      padding: 0px;
      margin: 0 0 0px;
      background-color: white;
      border: none;
      border-radius: 0px;
      font-family: monospace;
    }*/

    .orderReviewBtn{
      background: url('<?php echo e(asset('images/notification.png')); ?>') no-repeat;
      background-size: 90%; 
      width: 50px;
      height: 60px;
      margin: 20px 30px;
      border: none;
      background-color: transparent;
    } 

    .orders{
      background-color:#d92e2e;
      border-radius: 15px;
      padding: 4px 8px;
      color: #fff;
      position: absolute;
      margin: -30px 0 0 5px;
    }

    #logOutBtn{
      background: url('<?php echo e(asset('images/logout.png')); ?>') no-repeat;
      float:right;
      background-size: 100%; 
      width: 50px;
      height: 60px;
      margin: 20px 30px 0 0;
      border: none;
      background-color: transparent;
    }


    
    /* END of General CSS */

    /* Table Status Review */
    .tableStatus{
      text-align:center;
      padding: 5px;
    }

    .statusContainer{
      display: inline-block;
      width: 23%;
      text-align: left;
      margin: 0 25px;
    }

    .dot{
      margin: 5px;
      width: 28px;
      height: 28px;
      border-radius: 50%;
      display: inline-block;
      vertical-align:middle;
      box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16);
    }

    .green-dot{
      background-image: linear-gradient(180deg, #74f29a 0%, #00d96d 100%);
    }

    .grey-dot{
      background-image: linear-gradient(180deg, #777b90 0%, #5d6174 100%);
    }

    .red-dot{
      background-image: linear-gradient(180deg, #f86376 0%, #d92e2e 100%);
    }

    .blue-dot{     
      background-image: linear-gradient(180deg, #91a3ff 0%, #3152ea 100%);
    }
    /*END of Table Status Review */

    /* Radio Button */
    .tableSelectionContainer input[type="radio"] {
      clip: rect(0, 0, 0, 0);
      height: 1px;
      width: 1px;
      border: 0;
    }

    .tableSelectionContainer label {
      width: 80px;
      height: 70px;
      margin: 15px 15px;
      text-align: center;
      border-radius: 18px;
      font-size: 20px;
      padding-top: 20px;
    } 

    .tableSelectionContainer input[type="radio"]:checked+label {
      background-image: linear-gradient(180deg, #74f29a 0%, #00d96d 100%); 
    }

    .redBtn{
      color: #fff;
      background-image: linear-gradient(180deg, #f86376 0%, #d92e2e 100%);
      cursor: pointer;  
    }

    .greenBtn{
      color: #fff;
      background-image: linear-gradient(180deg, #777b90 0%, #5d6174 100%);
      cursor: pointer;  
    }

    .blueBtn{
      color: #fff;
      background-image: linear-gradient(180deg, #91a3ff 0%, #144fd3 100%);
      cursor: pointer;
    } 
    /* End of Radio Button */

    /* Loader */
    #loader {
      position: fixed;
      display: none;
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: rgba(0,0,0,0.5);     
      z-index:1000;
    }

    #loader-inner{
      position: absolute;
      left: 50%;
      top: 50%;
      z-index: 1000;
      margin: -75px 0 0 -55px;
      border: 16px solid #f3f3f3;
      border-radius: 50%;
      border-top: 16px solid #3498db;
      width: 100px;
      height: 100px;
      -webkit-animation: spin 2s linear infinite; /* Safari */
      animation: spin 2s linear infinite;
    }

    /* Safari */
    @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes  spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }
    /* END Loader */

    /* Footer Buttons */
    #yesToUnlockBtn,
    #noToUnlockBtn,
    #currentTableBtn,
    #cancelTableBtn,
    #newTableBtn{
      border: none;
      color: #fff;
      font-family: 'Avenir Heavy';
      font-size: 18px;
      text-transform: uppercase;
      transition: 0.2s ease;
      cursor: pointer;
      padding: 15px 30px;
      margin: 15px 10px;
      box-shadow: 0 5px 10px rgba(93, 97, 116, 0.27);
      border-radius: 60px;
      
    }

    #yesToUnlockBtn,
    #currentTableBtn{
      background-image: linear-gradient(180deg, #777b90 0%, #5d6174 100%);
    }

    #noToUnlockBtn,
    #cancelTableBtn{
      background-image: linear-gradient(180deg, #f76161 0%, #e51212 100%);
    }

    #newTableBtn{
      background-image: linear-gradient(180deg, #74f29a 0%, #00d96d 100%);
    }

    #submitBtn,#guestCheckBtn{
      border: none;
      color: #fff;
      font-family:'Avenir Heavy';
      font-size:25px;
      text-transform: uppercase;
      transition: 0.2s ease;
      cursor: pointer;
      padding: 25px 100px;
      margin:40px;
      box-shadow: 0 5px 10px rgba(93, 97, 116, 0.27);
      border-radius: 60px;
      background-image: linear-gradient(180deg, #777b90 0%, #5d6174 100%);
    }

    #submitBtn:hover,
    #submitBtn:focus,
    #guestCheckBtn:hover,
    #guestCheckBtn:focus{
      background: #587286;
      transition: 0.2s ease;
    }
    /* END of Footer Button */

    /* Order Review */
    .orderReviewTop {
      height: 50px;
    }

    h3.orderReviewTitle {
      text-align: center;
      width: 100%;
      position: absolute;
      margin-top: 15px;
      z-index:2;
    }

    .sidenav {
      height: 100%;
      width: 0;
      position: fixed;
      z-index: 1;
      top: 0;
      right: 0;
      background-color: #fff;
      overflow-x: hidden;
      transition: 0.5s;
    }

    .sidenav a {
      padding: 8px 8px 8px 32px;
      text-decoration: none;
      font-size: 25px;
      color: #818181;
      display: block;
      transition: 0.3s;
    }

    .sidenav a:hover {
      color: #f1f1f1;
    }

    .sidenav .closebtn {
      padding: 0;
      font-size: 36px;
      padding-right: 20px;
      z-index: 3;
      position: absolute;
      right: 0;
    }

    .orderContent{
      width:100%;
      box-shadow: 2px 3px 7px rgba(0, 0, 0, 0.5);
      border-radius: 20px;
      background-color: #ffffff;
      padding: 10px 20px;
      position:relative;
      min-height: 110px;
      margin: 15px 0;
    }

    .orderContainer{
      padding: 15px 15px;
    }

    .orderStatuslbl{
      border-radius: 26px;
      border: 3px solid #f86376;
      color: #f86376;
      position: absolute;
      right: 0;
      margin: 5px 15px;
      padding: 5px 20px;
      font-size: 16px;
    }
      
    .orderRemarklbl{     
      list-style-type: circle;  
      padding-left: 17px;
    }

    .reviewTableNumberlbl{
      border-radius: 15px;
      border: 3px solid #5d6174;
      padding: 5px 20px;
      top: 0;
      margin: 5px 0;
      font-size: 16px;
    }

    @media  screen and (max-height: 450px) {
      .sidenav {padding-top: 15px;}
      .sidenav a {font-size: 18px;}
    }
  /* END of Review Order */

  /* Notification */
    .modal-content{
      border-radius: 20px;
      border: 1px solid #707070;
    }

    .okBtn, .noBtn{
      border: none;
      color: #fff;
      font-family: 'Avenir Heavy';
      font-size: 25px;
      text-transform: uppercase;
      transition: 0.2s ease;
      cursor: pointer;
      padding: 10px 50px;
      margin: 25px 0;
      box-shadow: 0 5px 10px rgba(93, 97, 116, 0.27);
      border-radius: 60px;
      background-image: linear-gradient(180deg, #777b90 0%, #5d6174 100%);
    }

    .confirmlogoutBtn{
      font-family: 'Avenir Heavy';
      width: 330px;
      font-size: 25px;
      transition: 0.2s ease;
      cursor: pointer;
      padding: 5px 50px;
      margin: 35px 0 0;
      box-shadow: 0 5px 10px rgba(93, 97, 116, 0.27);
      border-radius: 60px;
      border: 4px solid #707070;
      background-color: #ffffff;
    }

    .confirmlogoutBtn:hover{
      background-image: linear-gradient(180deg, #8b8d9b 0%, #9e9fa5 100%);
      color:#ffffff;
    }

    .noBtn{
      width: 330px;
    }

    .modal-header {
      padding: 15px 22px 0;
      border-bottom: 0;
    }

    .modal-body {
      padding: 20px 30px;
    }
  /* END of Notification */
  </style>
</head>
<body>
<div id="loader" style="display:none;">
  <div id="loader-inner"></div>
</div>
<div id="outlet">
  <div class="bodyTop">
    <button type="button" class="orderReviewBtn" name="orderReviewBtn" id="orderReviewBtn" onclick="openNav()"><span class="orders" style='display:none;'></span></button>
    <button type="button" class="logOutBtn" name="logOutBtn" id="logOutBtn"></button>
  </div>
  <div class="selectFloorDiv" style="text-align: center;">
    <div style="display: inline-block;margin: auto;width: 100%;">
      <h2>Select Floor</h2>
      <div class="select-container">
        <select id="floorSelection" name="floor">
          <option disabled selected value> Select Floor</option>
        </select>
      </div>
    </div>
  </div>
  <div class="selectionContainer">
    <h2>Select Table</h2>
    <div class="tableStatus">
      <div class="tableStatus-top">
        <div class="statusContainer"><span class="dot green-dot"></span><label>Selected</label></div>
        <div class="statusContainer"><span class="dot grey-dot"></span><label>Available</label></div>
      </div>
      <div class="tableStatus-bottom">
        <div class="statusContainer"><span class="dot red-dot"></span><label>Occupied</label></div>
        <div class="statusContainer"><span class="dot blue-dot"></span><label>Locked</label></div>
      </div>
    </div>
    <div class="tableSelectionContainer"></div>
  </div>
  <div class="buttons">
    <!-- <button type="button" name="guestCheckBtn" id="guestCheckBtn">Preview Guest Check</button> -->
    <button type="button" name="submitBtn" id="submitBtn">Continue</button>
  </div>
</div>

<!-- Occupied Modal -->
<div class="modal fade" id="tableModal" tabindex="-1" role="dialog" aria-labelledby="tableModalLabel">
  <div class="modal-dialog" role="document" style="width:70% !important;">
    <div class="modal-content" style="width:100%;">
      <div class="modal-body" style="padding: 55px 35px;">
        <div class="tableModal" >
          <form id="tableForm" method="POST">
            <h3 id="tableTitle"><b></b></h3>
            <button type="button" name="currentTableBtn" id="currentTableBtn">Current Table</button>
            <button type="button" name="newTableBtn" id="newTableBtn">New Table</button>
            <button type="button" name="cancelTableBtn" id="cancelTableBtn" data-dismiss="modal" aria-label="Close">Cancel</button>
          </form>
        </div>
       </div>
    </div>
  </div>
</div>
<!-- End Occupied Modal -->

<!-- Locked Modal -->
<div class="modal fade" id="unlockModal" tabindex="-1" role="dialog" aria-labelledby="unlockModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="closeModalBtn" id="closeModalBtn" data-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="tableModal" >
          <form id="tableForm" method="POST">
            <h3><b>Are you sure you want to unlock this table ?</b></h3>
            <button type="button" name="yesToUnlockBtn" id="yesToUnlockBtn">Yes</button>
            <button type="button" data-dismiss="modal" name="noToUnlockBtn" id="noToUnlockBtn">No</button>
          </form>
        </div>
       </div>
    </div>
  </div>
</div>
<!-- End Locked Modal -->

<!-- Notification Modal -->
<div class="modal fade" id="NotificationModal" tabindex="-1" role="dialog" aria-labelledby="tableModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="tableModal" >
          <img src="<?php echo e(asset('images/notification.png')); ?>" style="margin:auto;" width="50" height="50">
          <h3><b>The food is ready to pick up.</b></h3>
          <button type="button" class="okBtn" data-dismiss="modal" aria-label="Close">OK</button>
        </div>
       </div>
    </div>
  </div>
</div>
<!-- End Notification Modal -->

<!-- Preview Receipt -->
<div class="modal fade" id="previewModal" tabindex="-1" role="dialog" aria-labelledby="previewModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="previewModalTitle"></h4>
      </div>
        <div class="modal-body">
          <div id="previewTbl" style="font-family: monospace;"></div>
        </div>
      </div>
    </div>
  </div>
  <!-- End of Preview Receipt -->

  <!-- Logout Modal -->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="logoutModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="tableModal" >
          <form id="tableForm" method="POST">
            <h1><b>Oh no! You’re leaving…<br>Are You Sure?</b></h1>
            <button type="button" name="confirmlogoutBtn" class="confirmlogoutBtn">Yes, Log Me Out</button>
            <br>
            <button type="button" class="noBtn" data-dismiss="modal" aria-label="Close">No</button>
          </form>
        </div>
       </div>
    </div>
  </div>
</div>
<!-- End Logout Modal -->

  <!-- overlay -->
  <div class="overlay" onclick="closeNav()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>
  <!-- end overlay -->

<div id="mySidenav" class="sidenav">
  <div class="orderReviewTop">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <h3 class="orderReviewTitle">Order Review</h3> 
  </div>
  <div class="orderContainer">
  </div>
</div>

<script>
var APP_URL = <?php echo json_encode(url('/')); ?>;
var apikey = "<?php echo $_SESSION['apiKey'];?>";
getFloor();
//DeleteEmptyRcpTable();
<?php
$_SESSION['tableOccupied'] = null;
$_SESSION['tableOccupiedRcpNo'] = null;
$_SESSION['tableNameOccupied'] = null;
$_SESSION['tablename'] = null;
$_SESSION['table'] = null;
$_SESSION['status'] = null;
?>

// getOrderReview();
getReadyNotification()

window.setInterval(function(){
  getReadyNotification()
}, 15000);

$('#okBtn').click(function(event){
$('#NotificationModal').modal('toggle');
});

function openNav() {
  document.getElementById("mySidenav").style.width = "500px";
  document.getElementById("myOverlay").style.display = "block";
  getOrderReview();
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("myOverlay").style.display = "none";
  
}

function preventBack() { 
  window.history.forward(); 
}
setTimeout("preventBack()", 0);
window.onunload = function () { null };

$('#floorSelection').on('change',function(){
  var floor = $(this).val();
  getTable(floor);
});

$('#logOutBtn').click(function(event){
  $("#logoutModal").modal("toggle");
});

$('.confirmlogoutBtn').click(function(event){
  logout();
});


function logout(){
  document.getElementById("loader").style.display = "block";
  $.ajax({
    url: APP_URL+"/logout",
    type: 'GET',
    success: function(){
      document.getElementById("loader").style.display = "none";
      var url = APP_URL+'/login';
      window.location.replace(url);
    },error: function(){
      document.getElementById("loader").style.display = "none";
      //error
    }
  });
}

/* $('#guestCheckBtn').click(function(event){
  var table = $("input[name='tableSelection']:checked").val();
  var status = $("input[name='tableSelection']:checked").attr('data-status');
  var tblname = $("input[name='tableSelection']:checked").attr('data-tblName');
  getGuestCheck(table,tblname,status);
}); */

$('#submitBtn').click(function(event){
  var table = $("input[name='tableSelection']:checked").val();
  var status = $("input[name='tableSelection']:checked").attr('data-status');
  var tblname = $("input[name='tableSelection']:checked").attr('data-tblName');

  if(status == 'Occupied'){
    if (/[a-zA-Z]/.test(tblname)) {
      setOccupiedSession(table,status,tblname);
    }else{
      $('#currentTableBtn').data('table', table);
      $('#newTableBtn').data('table', table);
      $('#currentTableBtn').data('status', status);
      $('#currentTableBtn').data('tblname', tblname);
      $('#newTableBtn').data('status', status);
      $('#newTableBtn').data('tblname', tblname);
      $('#tableTitle').text('Do you want to modify current order or create new order in table '+ tblname +'?');
      $('#tableModal').modal('toggle');     
    }
  }else if(status == 'Locked'){
    $('#yesToUnlockBtn').data('table', table);
    $('#yesToUnlockBtn').data('status', status);
    $('#yesToUnlockBtn').data('tblname', tblname);
    $('#unlockModal').modal('toggle');
  }else if(status == 'Available'){
    proceedToHome(table,status,tblname);
  }
});


$('#currentTableBtn').click(function(event){
  var table = $(this).data('table');
  var status = $(this).data('status');
  var tblname = $(this).data('tblname');
  setOccupiedSession(table,status,tblname);
});

$('#newTableBtn').click(function(event){
  var table = $(this).data('table');
  var status = $(this).data('status');
  var tblname = $(this).data('tblname');
  proceedToHome(table,status,tblname);
});

function setOccupiedSession(table,status,tblname){
    document.getElementById("loader").style.display = "block";
    $.ajax({
      type: "POST",
      url: APP_URL+"/api/v1/setOccupiedSession",
      data: {'table':table ,'status':status,'tblname':tblname},
      success: function(data)
      {
        if(data.error == false){
          window.location.href = APP_URL+"/home";
        }else{
          alert(data.message);
          document.getElementById("loader").style.display = "none";
        }
      },error(){
        alert('Problem Occured. Please try again');
        document.getElementById("loader").style.display = "none";
      }
    });
};


function proceedToHome(table,status,tblname){
  document.getElementById("loader").style.display = "block";
    $.ajax({
      type: "POST",
      url: APP_URL+"/api/v1/saveSelectedOutlet",
      data: {'table':table ,'status':status,'tblname':tblname},
      success: function(data)
      {
        if(data.error == false){
          <?php Session::forget('tableOccupied') ?>
          <?php Session::forget('tableOccupiedRcpNo') ?>
          <?php Session::forget('tableNameOccupied') ?>
          window.location.href = APP_URL+"/home";
        }
        else{
          alert(data.message);
          location.reload();
          document.getElementById("loader").style.display = "none";
        }
      },error(){
        alert('Problem Occured. Please try again');
        document.getElementById("loader").style.display = "none";
      }
    });
  // }
};


function getFloor(){
  document.getElementById("loader").style.display = "block";
  $.ajax({
      type: "GET",
      url: APP_URL+"/api/v1/getFloors",
      data: $.param({}),
      success: function(data){   
        if(data.error == false){
          var returnedData = data;
          var data = data['data'];
          if(data.length > 0){
            var select = $('#floorSelection').empty();
            $.each(data, function(i,item) {
              select.append('<option value="' + item.FloorName + '">'+ item.FloorName + '</option>'); 
            });
            $("#floorSelection").val(data[0].FloorName).change();
            //getTable(data[0].FloorName);
          }else{
            var select = $('#floorSelection').empty();
            select.append('<option disabled selected value> Select floor</option>');
            $('.tableSelectionContainer').empty();
            html = '<h4 style="text-align:center;">No table.</h4>';
            $(".tableSelectionContainer").append(html); 
          }
        }else{
          var select = $('#floorSelection').empty();
          select.append('<option disabled selected value> Select floor</option>');
          $('.tableSelectionContainer').empty();
          html = '<h4 style="text-align:center;">No table.</h4>';
          $(".tableSelectionContainer").append(html); 
          alert(data.message);
        }
        document.getElementById("loader").style.display = "none";
      },error: function(){
        document.getElementById("loader").style.display = "none";
        var select = $('#floorSelection').empty();
        select.append('<option disabled selected value> Select floor</option>');
        $('.tableSelectionContainer').empty();
        html = '<h4 style="text-align:center;">No table.</h4>';
        $(".tableSelectionContainer").append(html); 
      }
  });
}

window.setInterval(function(){
  var floor = $('#floorSelection').val();
  getTable(floor)
}, 30000);

function getTable(floor){
  $.ajax({
      type: "GET",
      url: APP_URL+"/api/v1/getTables",
      data: $.param({'floor':floor}),
      success: function(data){
        if(data.error == false){
          var returnedData = data;
          var data = data['data'];
          if(data.length > 0){
            $('.tableSelectionContainer').empty();
            html = "";
            $.each(data, function(i,item) {
              if(item.status == 'Occupied'){
                html += '<input id="'+item.tblName+'" type="radio" name="tableSelection" value="'+item.tblsysid+'" data-status="'+item.status+'"data-tblName="'+item.tblName+'"><label for="'+item.tblName+'" class="redBtn">'+item.tblName+'</label>';
              }else if(item.status == 'Available'){
                html += '<input id="'+item.tblName+'" type="radio" name="tableSelection" value="'+item.tblsysid+'" data-status="'+item.status+'"data-tblName="'+item.tblName+'"><label for="'+item.tblName+'" class="greenBtn">'+item.tblName+'</label>';
              }else if(item.status == 'Locked'){
                html += '<input id="'+item.tblName+'" type="radio" name="tableSelection" value="'+item.tblsysid+'" data-status="'+item.status+'"data-tblName="'+item.tblName+'"><label for="'+item.tblName +'" class="blueBtn">'+item.tblName+'</label>';
              }
            });
            $(".tableSelectionContainer").append(html); 
          }else{
            $('.tableSelectionContainer').empty();
            html = '<h4 style="text-align:center;">No table.</h4>';
            document.getElementsByClassName("tableSelectionContainer").innerHTML = html; 
          }
        }else{
          $('.tableSelectionContainer').empty();
          html = '<h4 style="text-align:center;">No table.</h4>';
          $(".tableSelectionContainer").append(html);  
          alert(data.message);
        }
        document.getElementById("loader").style.display = "none";
      },error: function(){
        document.getElementById("loader").style.display = "none";
        $('.tableSelectionContainer').empty();
        html = '<h4 style="text-align:center;">No table.</h4>';
        $(".tableSelectionContainer").append(html); 
      }
  });
}

function getOrderReview(){
  $.ajax({
    type: "GET",
    url: APP_URL+"/api/v1/getOrderReview",
    success: function(data){
        if(data.error == false){
          var prepare = data['prepare'];
          var ready = data['ready'];
          var html ='';
          var x =0;
          $('.orderContainer').empty();
          if(ready && ready.length > 0){
            $('.orders').text(ready.length);
            $('.orders').css({'display':'inline-block'});
            $.each(ready, function( index, ready ) {
              x++
              html += '<div class="orderContent">';
              var status ='Ready';
              if(ready.PROD_NM_CH){
                  var ch_nm = ready.PROD_NM_CH;
                }else{
                  var ch_nm = ' ';
                }

                var remarks = ready.Remarks;
                var remark = remarks.split(',');

                html += '<label class="reviewTableNumberlbl">Table '+ ready.ORDER_NUMBER+'</label>';
                html += '<label class="orderStatuslbl" style="color:#72f718;border-color:#72f718">'+ status +'</label>';
                html += '<h4>'+ ready.PROD_NM +'</h4><h4>'+ ch_nm +'</h4>';
                html += '<ul class="orderRemarklbl">';
                remark.forEach(function(r) {
                  if(/\S/.test(r)){
                    html += '<li>'+ r +'</li>';
                  }
                });
                html += '</ul></div>';
            });
          }
          if(prepare && prepare.length > 0){
            $.each(prepare, function( index, prepare ) {
              x++
              var status ='Preapring';
              html += '<div class="orderContent">';
              if(prepare.PROD_NM_CH){
                  var ch_nm = prepare.PROD_NM_CH;
                }else{
                  var ch_nm = ' ';
                }

                  var remarks = prepare.Remarks;
                  var remark = remarks.split(',');

                html += '<label class="reviewTableNumberlbl">Table '+ prepare.ORDER_NUMBER+'</label>';
                html += '<label class="orderStatuslbl" style="color:#00d900;border-color:#00d900">'+ status +'</label>';
                html += '<label class="orderStatuslbl">'+ status +'</label>';
                html += '<h4>'+ prepare.PROD_NM +'</h4><h4>'+ ch_nm +'</h4>';
                html += '<ul class="orderRemarklbl">';
                remark.forEach(function(r) {
                  if(/\S/.test(r)){
                    html += '<li>'+ r +'</li>';
                  }
                });
                html += '</ul></div>';
            });
          }
          $('.orderContainer').append(html);
        }
    },error: function(){
      alert('Problem occur. Please try again');
    }
  });
}

function getReadyNotification(){
  $.ajax({
    type: "GET",
    url: APP_URL+"/api/v1/getOrderReview",
    success: function(data){
        if(data.error == false){
          var ready = data['ready'];
          if(ready && ready.length > 0){
            console.log(ready.length);
            $('.orders').text(ready.length);
            $('.orders').css({'display':'inline-block'});
            $('#NotificationModal').modal('toggle');
          }else{
            $('.orders').text(ready.length);
            $('.orders').css({'display':'none'});
          }
        }
    },error: function(){
      alert('Problem occur. Please try again');
    }
  });
}

function DeleteEmptyRcpTable(){
  $.ajax({
    type: "POST",
    url: APP_URL+"/api/v1/DeleteEmptyRcpTable",
    success: function(data){
        if(data.error == false){
          console.log(data.message);
        }else{
          console.log(data.message);
        }
    },error: function(){
      alert('Problem occur. Please try again');
    }
  });
}

$('#yesToUnlockBtn').click(function(event){
  var table = $(this).data('table');
  var status = $(this).data('status');
  var tblname = $(this).data('tblname');
  unlockTable(table,status,tblname);
});

function unlockTable(table,status,tblname){
  $.ajax({
    type: "POST",
    url: APP_URL+"/api/v1/unlockTable",
    data: {table:table ,status:status, tblname:tblname},
    success: function(data){
        if(data.error == false){
          var floor = $('#floorSelection').val();
          getTable(floor)
          $('#unlockModal').modal('toggle');
        }else{
          alert(data.message);
          $('#unlockModal').modal('hide');
        }
    },error: function(){
      alert('Problem occur. Please try again');
    }
  });
}


</script>
</body>
</html>

