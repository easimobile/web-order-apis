<?php
Route::get('/',array(function () {
    return redirect('login');
}));

Route::any('login',array('uses'=>'UserController@login'));

Route::group(array('middleware'=>['web']),function(){
	Route::get('home',array('as'=>'home','uses'=>'UserController@home'));
	Route::get('error',array('uses'=>'UserController@error'));
	Route::get('logout',array('uses'=>'UserController@logout'));
	Route::get('tableselection',array('as'=>'outletselection','uses'=>'UserController@outlet'));
});


Route::group(array('prefix' => 'api/v1'), function(){
	//Info selection page
	Route::post('saveSelectedOutlet',array('uses'=>'ApiController@saveSelectedOutlet'));
	Route::get('getTablesTest',array('uses'=>'ApiController@getTablesTest'));
	Route::post('getGuestCheck',array('uses'=>'ApiController@getGuestCheck'));
	
	//Product remark modal
	Route::post('getSelection',array('uses'=>'ApiController@getSelection'));	

	//Menu Page
	Route::get('getMenu',array('uses'=>'ApiController@getMenu'));	
	Route::post('holdBill',array('uses'=>'ApiController@holdBill'));

	Route::get('getProducts',array('uses'=>'ApiController@getProducts'));
	Route::post('getNewProducts',array('uses'=>'ApiController@getNewProducts'));
	Route::get('getDetails',array('uses'=>'ApiController@getDetails'));
	Route::get('getOrderItem',array('uses'=>'ApiController@getOrderItem'));
	Route::get('getOrderReview',array('uses'=>'ApiController@getOrderReview'));
	
	
	Route::get('checkCollectAndPrepare',array('uses'=>'ApiController@checkCollectAndPrepare'));
	Route::post('setOccupiedSession',array('uses'=>'ApiController@setOccupiedSession'));
	Route::post('checkAndDeleteUsingTable',array('uses'=>'ApiController@checkAndDeleteUsingTable'));
	Route::get('getOutletDetails',array('uses'=>'ApiController@getOutletDetails'));	
	Route::post('DeleteEmptyRcpTable',array('uses'=>'ApiController@DeleteEmptyRcpTable'));	
	Route::post('unlockTable',array('uses'=>'ApiController@unlockTable'));
	
	//logout
	Route::any('login',array('uses'=>'ApiController@login'));
	Route::get('logout',array('uses'=>'ApiController@logout'));
	Route::post('userInfo',array('uses'=>'ApiController@userInfo'));

	//void
	Route::post('saveTableVoid',array('uses'=>'ApiController@saveTableVoid'));
	Route::get('getTableVoid',array('uses'=>'ApiController@getTableVoid'));
	Route::get('getItemVoid',array('uses'=>'ApiController@getItemVoid'));
	Route::post('voidItem',array('uses'=>'ApiController@voidItem'));
});

