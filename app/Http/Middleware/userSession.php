<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use DB;

class userSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(isset($_SESSION['apiKey'])){
            return redirect('tableselection');
        }        
        return $next($request);
    }
}
