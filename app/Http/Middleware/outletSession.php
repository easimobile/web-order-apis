<?php

namespace App\Http\Middleware;

use App\Http\Controllers\ApiController as ApiController;
use Closure;
use Session;
use DB;

class outletSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        /* if(!Session::has('outletCode')){
            Session::flush();
            return redirect('login')->with('Session Timedout',"Your session has timedout.");
        } */
        if(!isset($_SESSION['apiKey'])){
            return redirect('login')->with('Session Timedout',"Your session has timedout. Please login again.");
        }else{     
            $apiKey = $_SESSION['apiKey'];

            $userCode = DB::table('apiAuth')->where('apiKey',$apiKey)->value('userId');

            if($userCode){
                $latestKey = DB::table('apiAuth')->where('userId',$userCode)->latest('createdAt')->value('apiKey');
                
                if($latestKey != $apiKey){
                   session_destroy();
                    return redirect('login')->with('fail',"You have been logged out because you've signed in on another device");
                }
                
            }else{
                session_destroy();
                return redirect('login')->with('fail','Error Occured');
            }
        }
        return $next($request);
    }

}
