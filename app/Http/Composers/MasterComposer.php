<?php 
namespace App\Http\Composers;
use Illuminate\Contracts\View\View;
use DB;
use Session;
use Route;
use stdClass;
use App\Http\Controllers\ApiController as ApiController;

class MasterComposer {

    public function compose(View $view)
    {   
        //Session::flush();
        $this->apiCtrl = new ApiController();
        $outletCode = Session::get('outletCode');
        $outlets = $this->apiCtrl->getOutlets();
        $outlets = $outlets->getData();
        $outlets = $outlets->data;
        
        if($outletCode){
            $products = $this->apiCtrl->getProducts($outletCode);
            $outlet = $this->apiCtrl->getOutletDetails($outletCode);
        }
        else{
            $products = array();
            $outlet = array();
        }

        if (Session()->has('idToken')) {
    		$userInfo = Session::get('userInfo');
    		$idToken = Session::get('idToken');
		}else{
            if(Session()->has('userInfo'))
                $userInfo = Session::get('userInfo');
            else
			    $userInfo = "";
			
            $idToken = "";
		}
        $view->with(compact('outlets','outlet','userInfo','idToken','products','outletCode'));
    }

}