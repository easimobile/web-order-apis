<?php
namespace App\Http\Controllers;

session_start();
use Illuminate\Routing\Controller as BaseController;
use DB;
use Request, View;
use Validator;
use GuzzleHttp\Client;
use Session;
use Response;
use Redirect;
use Log;
use File;
use Config;
use SimpleXMLElement;
use Exception;
use Illuminate\Support\Facades\Input;

date_default_timezone_set("Asia/Singapore");

class ApiController extends BaseController
{
	public function __construct(){
		// Initialize variables
		$this->secretEncryptKey = '3mP';
		$this->secretEncryptDbKey = 'EASIUtilities';
		$this->webService = env('WEB_SERVICE');
		$this->outletCode = $this->retrieveSysFlagSetting('AB1');
		$this->serviceCharge = $this->retrieveSysFlagSetting2('S03');
		$this->gst = $this->retrieveSysFlagSetting2('S04');	
		$this->posNo = Config::get('defaults.PosNo');
		$this->priceLevel = Config::get('defaults.priceLevel');
		$this->orderSlipGroup = $this->retrieveSysFlagSetting2('SK9');
		$this->rcpPath = env('RECEIPT_PATH');
		$this->orderSlipPath = env('ORDERSLIP_PATH');
		$this->productGroup = Config::get('defaults.PRODUCTGROUP');
		$this->orderNumber = $this->retrieveSysFlagSetting2('BL9');
		$this->orderSlipExtension = $this->retrieveSysFlagSetting2('BM2');
		$this->checkPrepareTime = $this->retrieveSysFlagSetting2('A08');
	}

	public function retrieveSysFlagSetting($flag){
        //for internal api use
		$setting = DB::table('sysflag')->where('flag',$flag)->value('setting');
		return $setting;
	}

	public function retrieveSysFlagSetting2($flag){
        //for internal api use
		$setting = DB::connection('sqlsrv2')->table('sysflag')->where('flag',$flag)->value('setting');
		return $setting;
	}

	public function _insertEventLogger($eventLoggerType, $eventLoggerFunction, $eventLoggerLog,$eventLoggerDestination){
        try{
            $eventLogArray = array('EventLoggerType'=>$eventLoggerType,'EventLoggerFunction'=>$eventLoggerFunction,'EventLoggerLog'=>$eventLoggerLog,'EventLoggerTime'=>date('Y-m-d H:i:s'),'EventLoggerDestination'=>$eventLoggerDestination);

            DB::table('eventLogger')->insert($eventLogArray);

        }catch(Exception $e){
             Log::error($e->getMessage());
        }
	}

	public function service($url,$xmlData){
		$headers = array(
			"POST HTTP/1.1",
			"Content-Type: text/xml; charset=utf-8",
			"SOAPAction: http://tempuri.org/GenerateOrderNumber",
			"Content-length:".strlen($xmlData)
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlData);

		$response = curl_exec($ch); 
		$error = curl_error($ch);
		curl_close($ch);

		if ($error)
		{
			return $error;

		}else{

		$ns = new SimpleXMLElement($response);
		$ns->registerXPathNamespace('GenerateOrderNumberResponse', 'http://tempuri.org/');
		$orderNo = $ns->xpath("//GenerateOrderNumberResponse:GenerateOrderNumberResult")[0];
        $response = (string)$orderNo;
		}
		return $response;
	}

	public static function ConnectDb($dbinfo)
    {
        config(['database.connections.KPT' => [
            'driver' => 'sqlsrv',
			'host' => $dbinfo['host'],
			'database' => $dbinfo['dbName'],
            'username' => $dbinfo['dbUserName'],
            'password' => $dbinfo['dbPassword']
        ]]);

        return DB::connection('KPT');
    }


	public function getTables(){
		$outlet = $this->outletCode;
		$floor = Request::get('floor');
		$query = "LEN(tblName),tblName";
		
		
		$tables=DB::table('tables')->select('tblName','tblsysid')->where('SA_OL_CD', $outlet)->where('FloorName',$floor)->orderByRaw($query)->get();
		$array=$tables;
		
		foreach($tables as $t){
			$occupiedTbls = DB::connection('sqlsrv2')->table('tablesmapset')->select('tblnamedefined as tblName','tblsysid',
							DB::raw("CASE WHEN Locked = '1' THEN 'Locked' ELSE 'Occupied' END AS status"))
							->where('tblsysid',$t->tblsysid)
							->orderby('tblnamedefined','desc')->get();
			
			if($occupiedTbls){
				unset($t->tblsysid);
				unset($t->tblName);
				$array = array_merge($array,$occupiedTbls);
			}else{
				$t->status='Available';
			}
		}
		
		$array = json_encode($array);
		$array = json_decode($array,true);
		$array = array_filter($array);
		sort($array);
		
		foreach($array as $key =>$a){
			$newArray[$key]['tblName']=$a['tblName'];
			$newArray[$key]['tblsysid']=$a['tblsysid'];
			$newArray[$key]['status']=$a['status'];
		}
		
	
		usort($newArray, array($this,'specialSort'));
		
		return Response::json(array(
			'error' => false,
			'code' => 200,
			'message' => 'Tables retrieve successfully',
			'data' =>$newArray
		));
	}

	static function specialSort($a, $b) { 
		return strnatcmp($a['tblName'], $b['tblName']);
	}

	public function getProducts($outletCode){
		$priceLevel = $this->priceLevel;
		$prod_gp = $this->productGroup;
		
		$menuCode = DB::table('dept as d')->select('d.dept_cd')
					->leftjoin('prod_mapping as p', 'p.'.$prod_gp,'=','d.dept_cd')
					->where('p.sa_out_cd',$outletCode)->orderBy('d.disp_order','asc')
					->first();
		
		$product = DB::table('product as p')->select('p.prod_cd','p.prod_sh_nm as prod_nm','pm.price_'.$priceLevel.' as price','p.prod_nm_ch','p.open_price','p.tax_1','p.tax_2','pi.img_filepath')
					->leftjoin('prod_mapping as pm','pm.prod_cd','=','p.prod_cd')
					->leftjoin('productimage as pi','pi.prod_cd','=','p.prod_cd')
					->where('pm.sa_out_cd',$outletCode)
					->where('pm.active','1')
					->where('pm.'.$prod_gp,$menuCode->dept_cd)
					->whereIn('p.prod_type', ['SS','SA'])
					->get();
							
		foreach($product as $p){
			$price=number_format($p->price, 2, '.', '');
			$p->price=$price;		
		}
		
		return $product;		
	}
	
	public function getNewProducts(){
		$outletcode = $this->outletCode;
		$priceLevel = $this->priceLevel;
		$prod_gp = $this->productGroup;
		$menu = Request::get('menu');

		$product = DB::table('product as p')->select('p.prod_cd','p.prod_sh_nm','pm.price_'.$priceLevel.' as price','p.prod_nm_ch','p.tax_1','p.tax_2','pi.img_filepath')
					->leftjoin('prod_mapping as pm','pm.prod_cd','=','p.prod_cd')
					->leftjoin('productimage as pi','pi.prod_cd','=','p.prod_cd')
					->where('pm.sa_out_cd',$outletcode)
					->where('pm.active','1')
					->where('pm.'.$prod_gp, $menu)
					->whereIn('p.prod_type', ['SS','SA'])
					->get();
				
		foreach($product as $p){
			$price = number_format($p->price, 2, '.', '');
			$p->price = $price;
		}		
		return Response::json(array(
				'error' => false,
				'code' => 200,
				'message' => "Get products successfully",
				'data' => $product
			));		
	}
	
	public function getMenu(){		
		$outletCode = $this->outletCode;
		$prod_gp = $this->productGroup;
		$menu = DB::table('dept as d')->select('d.dept_cd','d.dept_nm','d.dept_ch','d.disp_order')
				->leftjoin('prod_mapping as p', 'p.'.$prod_gp,'=','d.dept_cd')
				->where('p.sa_out_cd',$outletCode)
				->orderBy('d.disp_order','asc')->distinct()->get();

		return Response::json(array(
			'error' => false,
			'code' => 200,
			'message' => "Get menu Successfully",
			'data' => $menu
		));	
	}

	public function getSelection(){
		$outletcode = $this->outletCode;
		$prod_cd = Request::get('prod_code');
		$prod_gp = $this->productGroup;

		$selection = DB::table('prod_mapping as p')
					->leftjoin('deptkitchenrequest as d', 'p.'.$prod_gp,'=','d.DepartmentCode')
					->leftjoin('kitchenrequest as k', 'k.KitchenRequestCode','=','d.KitchenRequestCode')
					->select('d.KitchenRequestCode','k.KitchenRequestDescription')
					->where('p.prod_cd',$prod_cd)
					->where('p.sa_out_cd',$outletcode)
					->where('k.Active','1')
					->get();
		
		return Response::json(array(
			'error' => false,
			'code' => 200,
			'message' => "Get selections successfully",
			'data' => $selection
		));	
	}
	
	public function holdBill(){
		$rules = array(
			'tableName'  	=> 'required',
			'tblSysId'  	=> 'required',
            'total'    		=> 'required',
            'items'  		=> 'required'
	
        );

        $validator = Validator::make(Request::all(), $rules);
        if(!$validator->passes()){
			if(empty(Request::get('item'))){
				return Response::json(array(
					'error' => false,
					'code' => 400,
					'message' => 'Please add item(s)'
				));
			}else{
				$validationErrorString = implode(',',$validator->errors()->all());
				return Response::json(array(
					'error' => true,
					'code' => 400,
					'message' => $validationErrorString
				));
			}
		}

		$table = Request::get('tblSysId');
		$tablename = Request::get('tableName');
		$total = Request::get('total');
		$items = Request::get('items');
	
		$response = $this->submitOrder($table, $tablename, $total, $items);

		if (strpos($response, 'Error') !== false){
			return Response::json(array(
				'error' => true,
				'code' => 400,
				'message' => $response
			));	
		}else{
			return Response::json(array(
				'error' => false,
				'code' => 200,
				'message' => $response
			));	
		}	
	}
	
	public function submitOrder($table, $tablename, $total, $items){	
		$outletCode = $this->outletCode;
		$now = date("Y-m-d H:i:s");
		$date = date("Y-m-d");
		$time = date("H:i:s");
		$numberOfPax = 1;
				
		try{
			DB::connection('sqlsrv2')->beginTransaction();
			$checktable = DB::connection('sqlsrv2')->table('tablesmapset')->where('tblsysid',$table)->where('tblnamedefined', $tablename)->first();
			if(!$checktable){	
				$insertTms = array('tblSysID'=>$table,'tblNameDefined'=>$tablename,'tblStatus'=>'O','tblTrnTime'=>$time,'tblBillSettled'=>'N','tblEmpID'=>$tablename,'tblProcessed'=>'0','pos_no'=>'W','Locked'=>'0','remark'=>'');	
				DB::connection('sqlsrv2')->table('tablesmapset')->insert($insertTms);		
			}
			
			$newRcpNo = DB::connection('sqlsrv2')->table('tablesmapset')->where('tblsysid',$table)->where('tblnamedefined', $tablename)->value('tblRcpNo');
			$control = DB::connection('sqlsrv2')->table('control')->where('sa_out_cd',$outletCode)->select('web_rcp','co_cd')->first();	
			if(!$newRcpNo){
				$rcpNo = $control->web_rcp + 1;
				$str_length = 6;
				DB::connection('sqlsrv2')->table('control')->where('sa_out_cd',$outletCode)->update(array('web_rcp'=>$rcpNo));
				$newRcpNo = substr("000000{$rcpNo}", -$str_length);
			}

			$updateTms = array('tblNoOfPax'=>$numberOfPax,'tblRcpNo'=>$newRcpNo);		
			$tms = DB::connection('sqlsrv2')->table('tablesmapset')->where('tblnamedefined',$tablename)->where('tblSysID',$table)->update($updateTms);

			if($tms){
				$existheaders = DB::connection('sqlsrv2')->table('headers')->where('rcp_no',$newRcpNo)->get();
				if($existheaders){
					$updateArray = array('CO_CD'		=> $control->co_cd,
										'SA_OUT_CD'		=> $outletCode,
										'POS_NO'		=> $this->posNo,
										'SHF_NO'		=> '1',
										'STAFF_C_CD'	=> $tablename,
										'TRN_TYPE'		=> 'S',
										'RCP_NO'		=> $newRcpNo,
										'TRN_DT'		=> $now,
										'TRN_TM'		=> $now,
										'TRN_DT'		=> $now,
										'BN_DT'			=> $now,
										'TRN_ST_TM'		=> $now,
										'TAX_AMT_1'		=> '0.00000',
										'TAX_AMT_2'		=> '0.00000',
										'TAX_AMT_3'		=> 0,
										'TAX_AMT_4'		=> 0,
										'TBL_NO'		=> $table,
										'GRS_AMT'		=> $total,
										'EXCES_AMT'		=> '0.00000',
										'NO_PRINT'		=> 0,
										'P_LEVEL'		=> $this->priceLevel,
										'COVER_CT'		=> $numberOfPax,
										'CUSTID'		=> 'WLK',
										'REC_TYPE'		=> 'CASH',
										'TPCUP'			=> 'N',
										'discount'		=> '0.00',
										'round_amt'		=> '0.00000',
										'dep_refd'		=> '0.00000',
										'tip_amt'		=> '0.00000'
						);	

					DB::connection('sqlsrv2')->table('headers')->where('rcp_no',$newRcpNo)->update($updateArray);
				}else{
					$insertheader = array('CO_CD'		=> $control->co_cd,
										'SA_OUT_CD'		=> $outletCode,
										'POS_NO'		=> $this->posNo,
										'SHF_NO'		=> '1',
										'STAFF_C_CD'	=> $tablename,
										'TRN_TYPE'		=> 'S',
										'RCP_NO'		=> $newRcpNo,
										'TRN_DT'		=> $now,
										'TRN_TM'		=> $now,
										'TRN_DT'		=> $now,
										'BN_DT'			=> $now,
										'TRN_ST_TM'		=> $now,
										'TAX_AMT_1'		=> '0.00000',
										'TAX_AMT_2'		=> '0.00000',
										'TAX_AMT_3'		=> 0,
										'TAX_AMT_4'		=> 0,
										'TBL_NO'		=> $table,
										'GRS_AMT'		=> $total,
										'EXCES_AMT'		=> '0.00000',
										'NO_PRINT'		=> 0,
										'P_LEVEL'		=> $this->priceLevel,
										'COVER_CT'		=> $numberOfPax,
										'CUSTID'		=> 'WLK',
										'REC_TYPE'		=> 'CASH',
										'TPCUP'			=> 'N',
										'discount'		=> '0.00',
										'round_amt'		=> '0.00000',
										'dep_refd'		=> '0.00000',
										'tip_amt'		=> '0.00000'
										);
							
					DB::connection('sqlsrv2')->table('headers')->insert($insertheader);
				}
			
				$x = 1;	
				foreach($items as $r){			
					$productInfo = DB::connection('sqlsrv2')->table('product')->where('prod_cd',$r['product_code'])->select('kpt_no','prod_g01','prod_g02')->first();
					$tax = DB::table('product')->where('prod_cd',$r['product_code'])->select('TAX_1','TAX_2')->first();
					$prod_cost = DB::table('prod_mapping')->where('prod_cd',$r['product_code'])->value('prod_cost');
					$randomString = $this->generateRandomString();

					if(empty($prod_cost))
						$prod_cost = '0.00';

					// $existdetails = DB::connection('sqlsrv2')->table('details')->where('prod_cd',$r->product_code)->where('rcp_no',$newRcpNo)->where('link_CD',$r['linkCD'])->get();

					/* if($existdetails){
						DB::connection('sqlsrv2')->table('details')->where('prod_cd',$r['code'])->where('rcp_no',$newRcpNo)->where('link_CD',$r['linkCD'])->update(array('rcp_no'=>$newRcpNo));
						$x++;
					}else{ */
					$insertDetails = array('DET_PKEY'			=> $x,
											'CO_CD'				=> $control->co_cd,
											'SA_OUT_CD'			=> $outletCode,
											'POS_NO'			=> $this->posNo,
											'SHF_NO'			=> '1',
											'STAFF_C_CD'		=> $tablename,
											'RCP_NO'			=> $newRcpNo,
											'PROD_G01'			=> $productInfo->prod_g01,
											'PROD_G02'			=> $productInfo->prod_g02,
											'TRN_DT'			=> $now,
											'TRN_TM'			=> $now,
											'BN_DT'				=> $now,
											'KPT_NO'			=> $productInfo->kpt_no,
											'TAX_1'				=> $tax->TAX_1,
											'TAX_2'				=> $tax->TAX_2,
											'DISCOUNT'			=> 0,
											'TRN_TYPE'			=> 'S',
											'BUY_TYPE'			=> 'I',
											'P_LEVEL'			=> $this->priceLevel,
											'LINK_CD'			=> $randomString,
											'NATION_CD'			=> '',
											'NEW_ID'			=> $randomString,
											'SA_PRICE'			=> $r['product_price'],
											'PROD_CD'			=> $r['product_code'],
											'PROD_NM'			=> $r['product_name'],
											'SA_QTY'			=> $r['quantity'],
											'SA_AMOUNT'			=> $r['multotal'],
											'BARCODE'			=> $r['product_code'],
											'PREAMOUNT'			=> $r['multotal'],
											'Remarks'			=> $r['remarks'],
											'Remarks2'			=> '',
											'Remarks3'			=> '',
											'BypassCustomerID' 	=> 0,
											'PackageCode'		=> '',
											'ProductPackageID' 	=> '',
											'Sequence'			=> $x++,
											'SALES_P_ID'		=> $tablename,
											'prod_cost'			=> $prod_cost,
											'ContractPeriod' 	=> 0,
											'YouthIndicator'	=> 0
										);

					/* if($r['remarks']){
						$insertDetails['Remarks'] = implode(',',$r['remarks']);
					}else
						$insertDetails['Remarks'] = '';	 */

					DB::connection('sqlsrv2')->table('details')->insert($insertDetails);
					// }					
				}
				DB::connection('sqlsrv2')->commit();
				$this->printReceipt($newRcpNo, $tablename);
				
				$floorFilter = DB::connection('sqlsrv2')->table('sysflag')->where('flag','BL8')->value('Setting');
				$orderNoWS = DB::connection('sqlsrv2')->table('sysflag')->where('flag','AB8')->value('Setting');
				$printOrderSlip = DB::connection('sqlsrv2')->table('sysflag')->where('flag','Z70')->value('Setting');
				$floor_string = DB::connection('sqlsrv2')->table('sysflag')->where('flag','BL8')->value('String');
				$cust_floor = DB::table('tables')->where('tblSysID',$table)->value('FloorName');
				$floor = explode(",",$floor_string);

				if($floorFilter == "T"){
					if(in_array($cust_floor, $floor)){  
						if($orderNoWS == "T"){
							if($this->orderNumber == "T"){
								$orderNo = $tablename;
							}else{
								$url = $this->webService."?op=GenerateOrderNumber";
								
								$xmldata   ='<?xml version="1.0" encoding="utf-8"?>
								<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
								<soap:Body>
									<GenerateOrderNumber xmlns="http://tempuri.org/">
									<groupCode>ORD</groupCode>
									</GenerateOrderNumber>
								</soap:Body>
								</soap:Envelope>';

								$orderNo = $this->service($url,$xmldata);	
							}		
							$rcpNo = $newRcpNo;
							
							DB::connection('sqlsrv2')->table('details')->where('RCP_NO',$rcpNo)->whereNull('ORD_NO')->update(array('ORD_NO'=>$orderNo));

							$deptCd = DB::connection('sqlsrv2')->table('sysflag')->where('flag','SK9')->value('Setting');
							$detailsDptCd = DB::connection('sqlsrv2')->table('details')->leftjoin('product','product.prod_cd','=','details.prod_cd')->where('details.rcp_no',$rcpNo)->select('product.'.$deptCd)->distinct()->get();					
							if($detailsDptCd){
								foreach($detailsDptCd as $d){
									if(!empty($d->$deptCd)){
										try{
											$kpt='[KPT'.$d->$deptCd.']';
												
											$lines = file('EASICRYPT/EASI.ini', FILE_IGNORE_NEW_LINES);

											$result = array();
											$index = -1;
												foreach ($lines as $l) {
													if ($l == $kpt) {
														$index++;
													}
													$result[$index][] = $l;
												}
											
											$server = $this->decryptDbInfo(str_replace("1=","",$result[0][1]));
											$dbName = $this->decryptDbInfo(str_replace("2=","",$result[0][2]));
											$dbUserName = $this->decryptDbInfo(str_replace("3=","",$result[0][3]));
											$dbPassword = $this->decryptDbInfo(str_replace("4=","",$result[0][4]));
											$windowAuthenticate = $this->decryptDbInfo(str_replace("5=","",$result[0][5]));
											$crypt = $this->decryptDbInfo(str_replace("6=","",$result[0][6]));

											$dbinfo = array('host'=>$server,'dbName'=>$dbName,'dbUserName'=>$dbUserName,'dbPassword'=>$dbPassword);

											$dbConnection = $this->ConnectDb($dbinfo);

											$detailsInfo = DB::connection('sqlsrv2')->table('details')
															->leftjoin('product','product.prod_cd','=','details.prod_cd')
															->where('details.rcp_no',$rcpNo)
															->where(function($query) { $query->where('nation_cd', '')->orWhereNull('nation_cd');})
															->where('product.'.$deptCd,$d->$deptCd)
															->WhereNotNull('product.'.$deptCd)
															->where('details.ord_no',$orderNo)
															->get();

											foreach($detailsInfo as $d){
												$PROD_NM_CH = DB::table('product')->where('PROD_CD',$d->PROD_CD)->value('PROD_NM_CH');
												$insertOrderDetails = array('ORDER_NUMBER'=>$orderNo,'QTY'=>(int)$d->SA_QTY,'PROD_NM'=>$d->PROD_NM,'PROD_NM_CH'=>$PROD_NM_CH,'PAYMENT_DT'=>$now,'BN_DT'=>$date ,'SA_OUT_CD'=>$d->SA_OUT_CD,'RCP_NO'=>$rcpNo,'POS_NO'=>'W','PROD_CD'=>$d->PROD_CD,'Remarks'=>$d->Remarks,'LINK_CD'=>$d->LINK_CD);
												
												$existItem = $dbConnection->table('order_table')->where('link_cd',$d->LINK_CD)->where('RCP_NO',$rcpNo)->where('PROD_CD',$d->PROD_CD)->get();

												if(!$existItem)
													$dbConnection->table('order_table')->insert($insertOrderDetails);

											}
											DB::purge('KPT');	
										}catch(Exception $e){
											$dbConnection->rollback();
											Log::info($e);
											info($e);
											$this->_insertEventLogger('API', 'holdBill(KDS)',json_encode($e),'HoldBill(KDS)'); 
											return 'Error Occured('.$e.')';
										}						 
									}
								}						
							}else{
								return 'No Department Code. Fail to insert KDS.';		
							}

							if($printOrderSlip == "T"){
								$prod_group = $this->orderSlipGroup;
								$prod = DB::connection('sqlsrv2')->table('details')->where('rcp_no',$rcpNo)->select('prod_cd')->get();

								$all_prodGroup = array();
								foreach($prod as $p){	
									$p = json_decode(json_encode($p), true);
									$prodGroupCode = DB::table('product')->where('prod_cd', $p)->value($prod_group);
									array_push($all_prodGroup, $prodGroupCode);
								}

								$prod_g = array_unique($all_prodGroup);
								foreach($prod_g as $pg){
									if(!empty($pg)){
										$response = $this->printOrderSlipByGroup($newRcpNo,$orderNo,$pg);
									
										if($response == 1){
											foreach($prod as $p){	
												$p = json_decode(json_encode($p), true);			
												DB::connection('sqlsrv2')->table('details')->where('rcp_no',$rcpNo)->where('prod_cd', $p)->update(['NATION_CD' => 1]);
											}
										}
									}
								}
							}
						}
					}
						
				}else{
					if($orderNoWS == "T"){
						if($this->orderNumber == "T"){
							$orderNo = $tablename;
						}else{
							$url = $this->webService."?op=GenerateOrderNumber";
							
							$xmldata   ='<?xml version="1.0" encoding="utf-8"?>
							<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
							<soap:Body>
								<GenerateOrderNumber xmlns="http://tempuri.org/">
								<groupCode>ORD</groupCode>
								</GenerateOrderNumber>
							</soap:Body>
							</soap:Envelope>';

							$orderNo = $this->service($url,$xmldata);	
						}		
						$rcpNo = $newRcpNo;
						
						DB::connection('sqlsrv2')->table('details')->where('RCP_NO',$rcpNo)->whereNull('ORD_NO')->update(array('ORD_NO'=>$orderNo));

						$deptCd = DB::connection('sqlsrv2')->table('sysflag')->where('flag','SK9')->value('Setting');
						$detailsDptCd = DB::connection('sqlsrv2')->table('details')->leftjoin('product','product.prod_cd','=','details.prod_cd')->where('details.rcp_no',$rcpNo)->whereNotNull('details.kpt_no')->select('product.'.$deptCd)->distinct()->get();					
						if($detailsDptCd){
							foreach($detailsDptCd as $d){
								if(!empty($d->$deptCd)){
									try{
										$kpt='[KPT'.$d->$deptCd.']';
											
										$lines = file('EASICRYPT/EASI.ini', FILE_IGNORE_NEW_LINES);

										$result = array();
										$index = -1;
											foreach ($lines as $l) {
												if ($l == $kpt) {
													$index++;
												}
												$result[$index][] = $l;
											}
										
										$server = $this->decryptDbInfo(str_replace("1=","",$result[0][1]));
										$dbName = $this->decryptDbInfo(str_replace("2=","",$result[0][2]));
										$dbUserName = $this->decryptDbInfo(str_replace("3=","",$result[0][3]));
										$dbPassword = $this->decryptDbInfo(str_replace("4=","",$result[0][4]));
										$windowAuthenticate = $this->decryptDbInfo(str_replace("5=","",$result[0][5]));
										$crypt = $this->decryptDbInfo(str_replace("6=","",$result[0][6]));

										$dbinfo = array('host'=>$server,'dbName'=>$dbName,'dbUserName'=>$dbUserName,'dbPassword'=>$dbPassword);

										$dbConnection = $this->ConnectDb($dbinfo);

										$detailsInfo = DB::connection('sqlsrv2')->table('details')
														->leftjoin('product','product.prod_cd','=','details.prod_cd')
														->where('details.rcp_no',$rcpNo)
														->where(function($query) { $query->where('nation_cd', '')->orWhereNull('nation_cd');})
														->where('product.'.$deptCd,$d->$deptCd)
														->WhereNotNull('product.'.$deptCd)
														->where('details.ord_no',$orderNo)
														->get();

										foreach($detailsInfo as $d){
											$PROD_NM_CH = DB::table('product')->where('PROD_CD',$d->PROD_CD)->value('PROD_NM_CH');
											$insertOrderDetails = array('ORDER_NUMBER'=>$orderNo,'QTY'=>(int)$d->SA_QTY,'PROD_NM'=>$d->PROD_NM,'PROD_NM_CH'=>$PROD_NM_CH,'PAYMENT_DT'=>$now,'BN_DT'=>$date ,'SA_OUT_CD'=>$d->SA_OUT_CD,'RCP_NO'=>$rcpNo,'POS_NO'=>'W','PROD_CD'=>$d->PROD_CD,'Remarks'=>$d->Remarks,'LINK_CD'=>$d->LINK_CD);
											
											$existItem = $dbConnection->table('order_table')->where('link_cd',$d->LINK_CD)->where('RCP_NO',$rcpNo)->where('PROD_CD',$d->PROD_CD)->get();

											if(!$existItem)
												$dbConnection->table('order_table')->insert($insertOrderDetails);

										}
										DB::purge('KPT');		
									}catch(Exception $e){
										$dbConnection->rollback();
										Log::info($e);
										info($e);
										$this->_insertEventLogger('API', 'holdBill(KDS)',json_encode($e),'HoldBill(KDS)'); 
										return 'Error Occured('.$e.')';
									}					 
								}
							}						
						}else{
							return 'No Department Code. Fail to insert KDS.';					
						}
						
						if($printOrderSlip == 'T'){
							$prod_group = $this->orderSlipGroup;
							$prod = DB::connection('sqlsrv2')->table('details')->where('rcp_no',$rcpNo)->select('prod_cd')->get();

							$all_prodGroup = array();
							foreach($prod as $p){	
								$p = json_decode(json_encode($p), true);
								$prodGroupCode = DB::table('product')->where('prod_cd', $p)->value($prod_group);
								array_push($all_prodGroup, $prodGroupCode);
							}

							$prod_g = array_unique($all_prodGroup);
							foreach($prod_g as $pg){
								if(!empty($pg)){
									$response = $this->printOrderSlipByGroup($newRcpNo,$orderNo,$pg);
									if($response == 1){
										foreach($prod as $p){	
											$p = json_decode(json_encode($p), true);			
											DB::connection('sqlsrv2')->table('details')->where('rcp_no',$rcpNo)->where('prod_cd', $p)->update(['NATION_CD' => 1]);
										}
									}
								}
							}
						}
					}
				}
				return 'Added successfully';
			}else{
				return 'Fail to holdbill. Unable to update table.';
			}
		}catch(Exception $e){
			DB::connection('sqlsrv2')->rollback();
			Log::info($e);
			info($e);
			$this->_insertEventLogger('API', 'holdBill',json_encode($e),'HoldBill'); 
			return 'Error Occured('.$e.')';
		}
	}
	
	public function printOrderSlipByGroup($newRcpNo,$orderNo,$pg){
		$params = array($newRcpNo,$orderNo,$pg);
		$this->_insertEventLogger('API', 'printOrderSlipByGroupRequest',json_encode($params),'printOrderSlipByGroup');  
		$spCall = DB::connection('sqlsrv2')->select('SET NOCOUNT ON; exec spPrintOrderSlipByGroup ?,?,?',$params);	
		$this->_insertEventLogger('API', 'printOrderSlipByGroupResponse',json_encode($spCall),'printOrderSlipByGroup');  
		if(!empty($spCall)){
			$line='';
			$fileTime = date("YmdHis");
			foreach($spCall as $s){
				$combinedString = "";
				$s->ESC_CD = str_replace("?","",$s->ESC_CD);
				$escapeCodes = explode('#',$s->ESC_CD);
				foreach($escapeCodes as $escCode){
					$removedPlusString = explode('+',$escCode);
					foreach($removedPlusString as $string){
						if($string || $string === '0')
							$combinedString .= chr($string);
					}
				}
				$line.=$combinedString.''.$s->DETAILS."\r\n";					
				}

				if($this->orderSlipExtension == 'T'){
					$extension = DB::connection('sqlsrv2')->table('sysflag')->where('flag','BM2')->value('string');
					if($extension){
						file_put_contents($this->orderSlipPath.'OrderSlip'.$orderNo.'-'.$newRcpNo.'-'.$fileTime.'.'.$extension, $line);
					}else{
						file_put_contents($this->orderSlipPath.'OrderSlip'.$orderNo.'-'.$newRcpNo.'-'.$fileTime.'.rcp', $line);
					}
				}else{
					file_put_contents($this->orderSlipPath.'OrderSlip'.$orderNo.'-'.$newRcpNo.'-'.$fileTime.'.rcp', $line);
				}
			return 1;
		}
	}


	public function printReceipt($rcpNo,$tablename){
		$kptNo = DB::connection('sqlsrv2')->table('details')->where('rcp_no',$rcpNo)->where('PRT_KPT',0)->select('kpt_no')->distinct()->get();	
		$tableName = DB::connection('sqlsrv2')->table('tablesmapset')->where('tblrcpno',$rcpNo)->value('tblNameDefined');	
		foreach($kptNo as $k){
			if($k->kpt_no!='0'){
				$kptPort = DB::connection('sqlsrv2')->table('kpt as k')->join('kpt_mapping as km','km.port','=','k.port')->where('k.no',$k->kpt_no)->value('km.ext');
				$kptPorts = DB::connection('sqlsrv2')->table('kpt as k')->join('kpt_mapping as km','km.port','=','k.port')->where('k.no',$k->kpt_no)->pluck('km.ext');
				
				$params = array($rcpNo,'S',$kptPort,$tableName,'');
				$this->_insertEventLogger('API', 'spPrintKitchenReceiptRequest',json_encode(array('UserID'=>$tablename, 'SQL'=>$params)),'spPrintKitchenReceipt');  
				$spCall = DB::connection('sqlsrv2')->select('SET NOCOUNT ON; exec spPrintKitchenReceipt ?,?,?,?,?',$params);
				$this->_insertEventLogger('API', 'spPrintKitchenReceiptResponse',json_encode(array('UserID'=>$tablename, 'Result'=>$spCall)),'spPrintKitchenReceipt');  				
				$line='';
				$fileTime = date("YmdHis");
				foreach($spCall as $s){
					$combinedString = "";
					$s->ESC_CD = str_replace("?","",$s->ESC_CD);
					$escapeCodes = explode('#',$s->ESC_CD);
					foreach($escapeCodes as $escCode){
						$removedPlusString = explode('+',$escCode);
						foreach($removedPlusString as $string){
							if($string || $string === '0')
								$combinedString .= chr($string);
						}
					}
					$line.=$combinedString.''.$s->DETAILS."\r\n";					
				}

				$string_encoded = mb_convert_encoding($line, 'GB2312', 'UTF-8');
				foreach($kptPorts as $k){
					file_put_contents($this->rcpPath.$rcpNo.'-'.$kptPort.'-'.$fileTime.'.'.$k, $string_encoded);
				}
			}
		}
		
		return true;
	}

	public function generateRandomString($length = 32) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	

/* 	public function saveSelectedOutlet(){
		$table = Request::get('table');
		$status = Request::get('status');
		$tblname = Request::get('tblname');
		$apiKey = $_SESSION['apiKey'];	
		$userCode = DB::table('apiauth')->where('apikey',$apiKey)->value('userid');	
		$time = date("H:i:s");
		$tablename = DB::table('tables')->where('tblsysid',$table)->value('tblName');
		$exist = DB::connection('sqlsrv2')->table('tablesmapset')->where('tblNameDefined',$tblname)->where('tblsysid',$table)->first();

		if($exist){
			$checktable = DB::connection('sqlsrv2')->table('tablesmapset')->where('tblsysid',$table)->select('tblnamedefined')->orderBy('tblautoid', 'desc')->first();	
			if($checktable){	
				if($checktable->tblnamedefined == $tablename){
					$tablename = $checktable->tblnamedefined.'-A';
				}else{
					foreach (range('A', 'Z') as $char) {
						if($checktable->tblnamedefined == $tablename.'-'.$char){
							$tablename = ++$checktable->tblnamedefined;
							break;
						}
					}
				}
			}
		}

		$insertTms = array('tblSysID'=>$table,'tblNameDefined'=>$tablename,'tblStatus'=>'O','tblTrnTime'=>$time,'tblBillSettled'=>'N','tblEmpID'=>$userCode,'tblProcessed'=>'0','pos_no'=>'W','Locked'=>'1','remark'=>'');	
		DB::connection('sqlsrv2')->table('tablesmapset')->insert($insertTms);

		if($insertTms){
			if($table){
				$_SESSION['table'] = $table;
				$_SESSION['tablename'] = $tablename;
				$_SESSION['status'] = $status;

				return Response::json(array(
					'error' => false,
					'code' => 200,
					'message' =>'Data saved'
				));
			}
		}

		return Response::json(array(
			'error' => true,
			'code' => 400,
			'message' => 'Failed to save data'
		));	
	}
 */
	public function md5hash($keyPhrase){
		$key = mb_convert_encoding($keyPhrase, "ASCII");
		$key = md5($key,true);
    	$key .= substr($key, 0, 8);
    	return $key;
	}

	public function encryptPassword($toEncrypt = null){

        if(Request::get('toEncrypt'))
            $toEncrypt = Request::get('toEncrypt');

		$key = $this->md5hash($this->secretEncryptKey);
		$iv = "";

        $newEncrypted = openssl_encrypt($toEncrypt, 'des-ede3', $key, 0, $iv);

        return $newEncrypted;
	}

	public function decryptPassword($encrypted_pw = null){

        if(Request::get('encrypted_pw'))
            $encrypted_pw = Request::get('encrypted_pw');

        $key = $this->md5hash($this->secretEncryptKey);
        $iv = '';

		$decrypted_pw = openssl_decrypt($encrypted_pw, 'des-ede3', $key, 0, $iv);

        return $decrypted_pw;
	}

	public function decryptDbInfo($encrypted_string = null){

        $key = $this->md5hash($this->secretEncryptDbKey);
        $iv = '';

		$decrypted_string = openssl_decrypt($encrypted_string, 'des-ede3', $key, 0, $iv);

        return $decrypted_string;
	}


	public function login(){
		$rules = array(
			'usercode'    => 'required|exists:tables,tblName',
			'password'    => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		$validateErrors = array();	
		if($validator->passes()){	
			$usercode = Request::get('usercode');
			$password = Request::get('password');
			
			$user = DB::table('employee')->where('EMP_CD',$usercode)->where('Active',1)->first();		
			if(!$user){
					return Response::json(array(
						'error'     => true,
						'code'      => 400,
						'message'   => "Invalid User ID/Password"
					)); 
			}
			
			$decrypted_pw = $this->decryptPassword($user->EMP_PWD);
		
			if($decrypted_pw == $password){
				$tblSysId = DB::table('tables')->where('tblName',$usercode)->value('tblSysID');
				$data = array();
				$data['tableName'] = $usercode;				
				$data['tblSysId'] = $tblSysId;

				return Response::json(array(
                    'error' => true,
                    'code' => 200,
					'message' => 'login sucessfully',
					'data'	=> $data
				));
			}else{		
				$encrytKeyedPassword = $this->encryptPassword($password);
				$message = "Invalid User ID/Password";	
				return Response::json(array(
					'error'     => true,
					'code'      => 400,
					'message'   => $message
				)); 
			}
		}else{
			$validationErrorString = implode(',',$validator->errors()->all());
			return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
		}     
	}

	public function voidItem(){
		$rules = array(
			'prod_cd' 	=> 'required',
			'link_cd'  	=> 'required',
			'tblSysId'  => 'required',
			'tableName'	=> 'required'
		);

		$validator = Validator::make(Input::all(), $rules);
		if($validator->passes()){
			try{
				$prod_cd = Request::get('prod_cd');
				$linkCD = Request::get('link_cd');
				$tableName = Request::get('tableName');		
				$tblSysId = Request::get('tblSysId');						
				$now = date("Y-m-d H:i:s");

				$rcpNo = DB::connection('sqlsrv2')->table('tablesmapset')->where('tblSysID',$tblSysId)->where('tblnamedefined',$tableName)->value('tblRcpNo');

				DB::beginTransaction();				
				$detail = DB::connection('sqlsrv2')->table('details')->where('rcp_no',$rcpNo)->where('prod_cd', $prod_cd)->where('link_cd',$linkCD)->first();
				
				$arrayAuditLog = array('CO_CD'			=>'1',
										'SA_OUT_CD'		=>$this->outletCode,
										'POS_NO'		=>'W',
										'BN_DT'			=>$now,
										'RCP_NO'		=>$rcpNo,
										'TRN_TIME'		=>$now,
										'TIME_ACT'		=>$now,
										'TRN_TYPE'		=>'IV',
										'PROD_CD'		=>$prod_cd,
										'PROD_NM'		=>$detail->PROD_NM,
										'EMP_ID'		=>$tableName,
										'EMP_NM'		=>$tblSysId,
										'SA_AMOUNT'		=>$detail->SA_AMOUNT,
										'SA_QTY'		=>$detail->SA_QTY,
										'PRT_KPT'		=>'1',
										'BUY_TYPE'		=>'I',
										'KPT_NO'		=>$detail->KPT_NO,
										'TRN_NO'		=>$rcpNo,
										'SSIS_STATUS'	=>'1',
										'SALES_P_ID'	=>$tableName,
										'OldValue'		=>'',
										'NewValue'		=>'');

				$deptCd = DB::connection('sqlsrv2')->table('sysflag')->where('flag','SK9')->value('Setting');
				$detailsDptCd = DB::connection('sqlsrv2')->table('details')->leftjoin('product','product.prod_cd','=','details.prod_cd')->where('details.rcp_no',$rcpNo)->where('details.prod_cd', $prod_cd)->select('product.'.$deptCd)->first();
				if($detailsDptCd->$deptCd){
					$kpt = '[KPT'.$detailsDptCd->$deptCd.']';				
					$lines = file('EASICRYPT/EASI.ini', FILE_IGNORE_NEW_LINES);

					$result = array();
					$index = -1;
					foreach ($lines as $l) {
						if ($l == $kpt) {
							$index++;
						}
						$result[$index][] = $l;
					}
				
					$server = $this->decryptDbInfo(str_replace("1=","",$result[0][1]));
					$dbName = $this->decryptDbInfo(str_replace("2=","",$result[0][2]));
					$dbUserName = $this->decryptDbInfo(str_replace("3=","",$result[0][3]));
					$dbPassword = $this->decryptDbInfo(str_replace("4=","",$result[0][4]));
					$windowAuthenticate = $this->decryptDbInfo(str_replace("5=","",$result[0][5]));
					$crypt = $this->decryptDbInfo(str_replace("6=","",$result[0][6]));

					$dbinfo = array('host'=>$server,'dbName'=>$dbName,'dbUserName'=>$dbUserName,'dbPassword'=>$dbPassword);
					$dbConnection = $this->ConnectDb($dbinfo);
				
					if($this->checkPrepareTime == 'T'){
						$existOrderTable = $dbConnection->table('order_table')->where('rcp_no',$rcpNo)->where('prod_cd',$prod_cd)->whereNotNull('PREPARE_DT')->where('link_cd',$linkCD)->get();
						if(!$existOrderTable){
								$dbConnection->table('order_table')->where('rcp_no',$rcpNo)->where('prod_cd', $prod_cd)->where('link_cd',$linkCD)->delete();
						}else{
							DB::connection('sqlsrv2')->rollback();
							$message = 'This item already prepared.';
							return Response::json(array(
								'error'     => true,
								'code'      => 400,
								'message'   => $message
							));
						}
					}else{
						$existOrderTable = $dbConnection->table('order_table')->where('rcp_no',$rcpNo)->where('prod_cd',$prod_cd)->where('link_cd',$linkCD)->get();
						if($existOrderTable){
							$dbConnection->table('order_table')->where('rcp_no',$rcpNo)->where('prod_cd', $prod_cd)->where('link_cd',$linkCD)->delete();
						}else{	
							DB::connection('sqlsrv2')->rollback();
							$message = 'This item is not exist.';
							return Response::json(array(
								'error'     => true,
								'code'      => 400,
								'message'   => $message
							));
						}
					}
					DB::purge('KPT');							 
				}				
				DB::connection('sqlsrv2')->table('audit_log')->insert($arrayAuditLog);
				DB::connection('sqlsrv2')->table('details')->where('rcp_no',$rcpNo)->where('prod_cd', $prod_cd)->where('link_cd',$linkCD)->delete();
														
				$kptNo = DB::connection('sqlsrv2')->table('audit_log')->where('rcp_no',$rcpNo)->where('TRN_TYPE','IV')->where('PRT_KPT','1')->select('kpt_no')->distinct()->get();	
				$tableName = DB::connection('sqlsrv2')->table('tablesmapset')->where('tblrcpno',$rcpNo)->value('tblNameDefined');			
				foreach($kptNo as $k){
					if($k->kpt_no != '0'){
						$kptPort = DB::connection('sqlsrv2')->table('kpt as k')->join('kpt_mapping as km','km.port','=','k.port')->where('k.no',$k->kpt_no)->value('km.ext');
						$kptPorts = DB::connection('sqlsrv2')->table('kpt as k')->join('kpt_mapping as km','km.port','=','k.port')->where('k.no',$k->kpt_no)->pluck('km.ext');
						
						$params = array($rcpNo, $printType='V', $kptPort, $tableName, '');
						$spCall = DB::connection('sqlsrv2')->select('SET NOCOUNT ON; exec spPrintKitchenReceipt ?,?,?,?,?',$params);

						$line='';
						$fileTime = date("YmdHis");
						foreach($spCall as $s){
							$combinedString = "";
							$s->ESC_CD = str_replace("?","",$s->ESC_CD);
							$escapeCodes = explode('#',$s->ESC_CD);
							foreach($escapeCodes as $escCode){
								$removedPlusString = explode('+',$escCode);
								foreach($removedPlusString as $string){
									if($string || $string === '0')
										$combinedString .= chr($string);
								}
							}							
							$line .= $combinedString.''.$s->DETAILS."\r\n";					
						}

						$string_encoded = mb_convert_encoding($line, 'GB2312', 'UTF-8');
						foreach($kptPorts as $k){
							file_put_contents($this->rcpPath.$rcpNo.'-'.$kptPort.'(VOID)'.$fileTime.'.'.$k, $string_encoded);
						}
					}
				}			

				$total = DB::connection('sqlsrv2')->table('details')->where('rcp_no',$rcpNo)->sum('sa_amount');
				$updatearray = array('grs_amt'=>$total);
				DB::connection('sqlsrv2')->table('headers')->where('rcp_no',$rcpNo)->update($updatearray);
				
				$message = 'Item voided successfully';
				return Response::json(array(
					'error'     => false,
					'code'      => 200,
					'message'   => $message
				));			
			}catch(Exception $e){
				Log::info($e->getMessage());
				info($e->getMessage());
				$this->_insertEventLogger('API', 'VoidItem',json_encode($e),'VoidItem'); 
				return Response::json(array(
					'error'     => true,
					'code'      => 500,
					'message'   => $e->getMessage()
				)); 
			}
		}else{
			$validationErrorString = implode(',',$validator->errors()->all());
			return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
		}	
	}

	public function getOrderItem(){
		$rules = array(
			'tblSysId'  => 'required',
			'tableName'	=> 'required'
		);

		$validator = Validator::make(Input::all(), $rules);
		if($validator->passes()){	
			$tblSysId = Request::get('tblSysId');
			$tableName = Request::get('tableName');
			$rcpNo = DB::connection('sqlsrv2')->table('tablesmapset')->where('tblsysid',$tblSysId)->where('tblnamedefined',$tableName)->value('tblrcpno');
			
			if($rcpNo){
				$data = DB::connection('sqlsrv2')->table('details')->select('PROD_CD','PROD_NM','SA_QTY as quantity','remarks','SA_PRICE as prod_price','SA_AMOUNT as multotal','link_cd')->where('RCP_NO', $rcpNo)->get();
				if($data){
					foreach($data as $d){
						$d->quantity = (int)$d->quantity;
						$d->PROD_NM_CH = DB::table('product')->where('prod_cd',$d->PROD_CD)->value('prod_nm_ch');
						// $d->svc = DB::table('product')->where('prod_cd',$d->PROD_CD)->value('tax_1');
						// $d->gst = DB::table('product')->where('prod_cd',$d->PROD_CD)->value('tax_2');
					}

				return Response::json(array(
					'error'     => false,
					'code'      => 200,
					'message'   => 'Order retrieve successfully',
					'data'		=> $data
				));
				}
			}
			return Response::json(array(
				'error'     => false,
				'code'      => 200,
				'message'   => 'No order yet.'
			)); 
		}else{
			$validationErrorString = implode(',',$validator->errors()->all());
			return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
		}
	}

	/* public function setOccupiedSession(){
		$table = Request::get('table');
		$tblname = Request::get('tblname');
		$checkLock = DB::connection('sqlsrv2')->table('tablesmapset')->where('tblNameDefined',$tblname)->where('tblsysid',$table)->value('Locked');
		if($checkLock == 0){
			$tableRcpNo = DB::connection('sqlsrv2')->table('tablesmapset')->where('tblsysid',$table)->where('tblnamedefined',$tblname)->value('tblRcpNo');
			if($tableRcpNo){
				
				$_SESSION['tableNameOccupied'] = $tblname;
				$_SESSION['tableOccupied'] = $table;
				$_SESSION['tableOccupiedRcpNo'] = $tableRcpNo;
				DB::connection('sqlsrv2')->table('tablesmapset')->where('tblNameDefined',$tblname)->where('tblsysid',$table)->update(array('Locked'=>'1'));
				
				return Response::json(array(
					'error'     => false,
					'code'      => 200,
					'message'	=>'proceed'
				));
			}
		}else{
			return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => "This table is ordering."
            ));
		}
	} */

	/* public function checkAndDeleteUsingRcpNo($rcpNo){
		$existItem = DB::connection('sqlsrv2')->table('details')->where('RCP_NO',$rcpNo)->get();
		if(!$existItem){
			DB::connection('sqlsrv2')->table('tablesmapset')->where('tblrcpno',$rcpNo)->delete();
			DB::connection('sqlsrv2')->table('headers')->where('RCP_NO',$rcpNo)->delete();
		}else{
			DB::connection('sqlsrv2')->table('tablesmapset')->where('RCP_NO',$rcpNo)->update(array('Locked'=>'0'));
		}
		return 0;
	}

	public function DeleteEmptyRcpTable(){
		try{
		DB::connection('sqlsrv2')->table('tablesmapset')->whereNull('tblrcpno')->delete();
		return Response::json(array(
			'error' => false,
			'code' => 200,
			'message' => "Delete Succesfully"
		));
		}catch(exception $e){
			return Response::json(array(
                'error'     => true,
                'code'      => 500,
                'message'   => $e
            )); 
        }
	} */

	/* public function checkAndDeleteUsingTable(){
		if(isset($_SESSION['table']))
			$table = $_SESSION['table'];

		if(isset($_SESSION['tablename']))
			$tablename = $_SESSION['tablename'];

		if(isset($_SESSION['tableOccupied']))
			$table = $_SESSION['tableOccupied'];

		if(isset($_SESSION['tableNameOccupied']))
			$tablename = $_SESSION['tableNameOccupied'];

			if(isset($table, $tablename)){
				$rcpNo = DB::connection('sqlsrv2')->table('tablesmapset')->where('tblsysid',$table)->where('tblnamedefined',$tablename)->value('tblrcpno');
				if(!$rcpNo){
					DB::connection('sqlsrv2')->table('tablesmapset')->where('tblsysid',$table)->where('tblnamedefined',$tablename)->delete();
				}else{
					$existItem = DB::connection('sqlsrv2')->table('details')->where('RCP_NO',$rcpNo)->get();
					if(!$existItem){
						DB::connection('sqlsrv2')->table('tablesmapset')->where('tblrcpno',$rcpNo)->orwhere('tblnamedefined',$tablename)->delete();
						DB::connection('sqlsrv2')->table('headers')->where('RCP_NO',$rcpNo)->delete();
					}else{
						DB::connection('sqlsrv2')->table('tablesmapset')->where('tblNameDefined',$tablename)->where('tblsysid',$table)->update(array('Locked'=>'0'));
					}
				}
			}
			return Response::json(array(
				'error'     => false,
				'code'      => 200
			));
	} */

	public function getGuestCheck(){
		$rules = array(
			'tblSysId'  => 'required',
			'tableName'	=> 'required'
		);

		$validator = Validator::make(Input::all(), $rules);
		if($validator->passes()){	
			$tblName = Request::get('tableName');
			$tblsysid = Request::get('tblSysId');

			try{
				$rcpNo = DB::connection('sqlsrv2')->table('tablesmapset')->where('tblNamedefined', $tblName)->where('tblSysId', $tblsysid)->value('tblRcpNo');
				if($rcpNo){
					$product = DB::connection('sqlsrv2')->table('details')
								->select('TRN_TYPE','PROD_CD','PROD_G01','PROD_G02','PROD_NM','SA_QTY','SA_PRICE','SA_AMOUNT','PROD_COST','DISCOUNT','BARCODE','TAX_1','TAX_2','TAX_3','TAX_4','P_LEVEL','PROD_TYPE','PROMO_CD as PROM_CD','PROM_TYPE','LINK_CD','PROMO_MSG','NORMAL_PRICE','SAVING','NETT','DISC_CD','PROD_COLOR','PROD_SIZE','BUY_TYPE','NATION_CD','CustCD','PreAmount','prt_kpt','sales_p_id','KptStatus','SALES_P_2','SALES_P_3','DiscRemark','SerialNo','ServiceNo','PlanCode','CustomerID','CustomerName','ContractPeriod','PromotionCode','QueueNo','DELIndicator','PromoGroup','CommentCode','YouthIndicator','ConnectionType','Remarks','Remarks2','Remarks3','BypassCustomerID','PackageCode','ProductPackageID','TRN_DT','TRN_TM')
								->where('RCP_NO', $rcpNo)
								->get();
					
					$productarray = json_encode($product);
					$productarray = json_decode($productarray,true);
					DB::connection('sqlsrv2')->table('temp_detail')->truncate();

					if($product){
						foreach($productarray as $p){
							$p['PROM_CD']='';
							$p['PROM_TYPE']='';
							$p['PROMO_MSG']='';
							$p['OtherPromType']='';
							$p['OtherPromCode']='';
							$p['PromoQualified']='';
							
							DB::connection('sqlsrv2')->table('temp_detail')->insert($p);
						}
				
						DB::connection('sqlsrv2')->table('temp_detail2')->truncate();
						DB::connection('sqlsrv2')->table('final_detail')->truncate();				
						DB::connection('sqlsrv2')->select('SET NOCOUNT ON; exec spProcessTempDetail');
						DB::connection('sqlsrv2')->statement('SET NOCOUNT ON; exec spPreparePromotionTable');
						$params = array('','','D','0','');
						DB::connection('sqlsrv2')->select('SET NOCOUNT ON; exec spPromotion ?,?,?,?,?',$params);
						$params1 = array($rcpNo,'R','',$tblName);
						$guestCheck = DB::connection('sqlsrv2')->select('SET NOCOUNT ON;exec spPrintGuestCheck ?,?,?,?',$params1); 

						foreach($guestCheck as $d){
							$data[] = $d->DETAILS;
						}
						
						return Response::json(array(
							'error' => false,
							'code' => 200,
							'message' => 'Guest check retrieve succecfully',
							'data' => $data
						));
					}
				}
				return Response::json(array(
					'error' => true,
					'code' => 402,
					'message' => 'This table have no order yet.'
				));	
			}catch(Exception $e){
				Log::info($e->getMessage());
				info($e->getMessage());
				$this->_insertEventLogger('API', 'GuestCheck',json_encode($e),'GuestCheck'); 
				return Response::json(array(
					'error'     => true,
					'code'      => 500,
					'message'   => $e->getMessage()
				)); 
			}
		}else{
			$validationErrorString = implode(',',$validator->errors()->all());
			return Response::json(array(
                'error' => true,
                'code' => 400,
                'message' => $validationErrorString
            ));
		}
	}

	/* public function getOrderReview(){
		$apiKey = $_SESSION['apiKey'];
		$userCode = DB::table('apiauth')->where('apikey',$apiKey)->value('userid');
		$userTable = DB::connection('sqlsrv2')->table('tablesmapset')->where('tblEmpid',$userCode)->get();
		
		if($userTable){
			foreach($userTable as $u){
				$rcpNo = DB::connection('sqlsrv2')->table('tablesmapset')->where('tblsysid',$u->tblSysID)->where('tblnamedefined',$u->tblNameDefined)->value('tblrcpno');
				$deptCd = DB::connection('sqlsrv2')->table('sysflag')->where('flag','SK9')->value('Setting');
				$detailsDptCd = DB::connection('sqlsrv2')->table('details')->where('rcp_no',$rcpNo)->whereNotNull('kpt_no')->select($deptCd)->distinct()->get();
				$orderNo = DB::connection('sqlsrv2')->table('details')->where('rcp_no',$rcpNo)->select('ord_No')->distinct()->get();			
				if($detailsDptCd){
					foreach($detailsDptCd as $d){

						$kpt='[KPT'.$d->$deptCd.']';
							
						$lines = file('EASICRYPT/EASI.ini', FILE_IGNORE_NEW_LINES);

							$result = array();
							$index = -1;
								foreach ($lines as $l) {
									if ($l == $kpt) {
										$index++;
									}
									$result[$index][] = $l;
								}
							
							$server = $this->decryptDbInfo(str_replace("1=","",$result[0][1]));
							$dbName = $this->decryptDbInfo(str_replace("2=","",$result[0][2]));
							$dbUserName = $this->decryptDbInfo(str_replace("3=","",$result[0][3]));
							$dbPassword = $this->decryptDbInfo(str_replace("4=","",$result[0][4]));
							$windowAuthenticate = $this->decryptDbInfo(str_replace("5=","",$result[0][5]));
							$crypt = $this->decryptDbInfo(str_replace("6=","",$result[0][6]));

							$dbinfo = array('host'=>$server,'dbName'=>$dbName,'dbUserName'=>$dbUserName,'dbPassword'=>$dbPassword);

							$dbConnection = $this->ConnectDb($dbinfo);

							foreach($orderNo as $o){
								$prepareInfo = $dbConnection->table('order_table')->where('rcp_no',$rcpNo)->where('order_number',$o->ord_No)->whereNull('prepare_dt')->whereNull('collect_dt')->get();
								$readyInfo = $dbConnection->table('order_table')->where('rcp_no',$rcpNo)->where('order_number',$o->ord_No)->whereNotNull('prepare_dt')->whereNull('collect_dt')->get();
							}	
							DB::purge('KPT'); 							
					}
												
				}
			}
			if(isset($prepareInfo)||isset($readyInfo)){
				return Response::json(array(
					'error' => false,
					'code' => 200,
					'prepare' => $prepareInfo,
					'ready'=>$readyInfo
				));
			}else{
				return Response::json(array(
					'error' => true,
					'code' => 400,
					'message' => 'No order'
				));
			}
		}else{
			return Response::json(array(
				'error' => true,
				'code' => 400,
				'message' => 'No order'
			));
		}
	} */

	/* public function checkCollectAndPrepare(){
		$apiKey = $_SESSION['apiKey'];
		$userCode = DB::table('apiauth')->where('apikey',$apiKey)->value('userid');
		$userTable = DB::connection('sqlsrv2')->table('tablesmapset')->where('tblEmpid',$userCode)->get();
		
		if($userTable){
			foreach($userTable as $u){
				$rcpNo=DB::connection('sqlsrv2')->table('tablesmapset')->where('tblsysid',$u->tblSysID)->where('tblnamedefined',$u->tblNameDefined)->value('tblrcpno');
				$deptCd = DB::connection('sqlsrv2')->table('sysflag')->where('flag','SK9')->value('Setting');
				$detailsDptCd = DB::connection('sqlsrv2')->table('details')->where('rcp_no',$rcpNo)->whereNotNull('kpt_no')->select($deptCd)->distinct()->get();
				$orderNo = DB::connection('sqlsrv2')->table('details')->where('rcp_no',$rcpNo)->select('ord_No')->distinct()->get();
		
				if($detailsDptCd){
					foreach($detailsDptCd as $d){

						$kpt='[KPT'.$d->$deptCd.']';
							
						$lines = file('EASICRYPT/EASI.ini', FILE_IGNORE_NEW_LINES);

							$result = array();
							$index = -1;
								foreach ($lines as $l) {
									if ($l == $kpt) {
										$index++;
									}
									$result[$index][] = $l;
								}
							
							$server = $this->decryptDbInfo(str_replace("1=","",$result[0][1]));
							$dbName = $this->decryptDbInfo(str_replace("2=","",$result[0][2]));
							$dbUserName = $this->decryptDbInfo(str_replace("3=","",$result[0][3]));
							$dbPassword = $this->decryptDbInfo(str_replace("4=","",$result[0][4]));
							$windowAuthenticate = $this->decryptDbInfo(str_replace("5=","",$result[0][5]));
							$crypt = $this->decryptDbInfo(str_replace("6=","",$result[0][6]));

							$dbinfo = array('host'=>$server,'dbName'=>$dbName,'dbUserName'=>$dbUserName,'dbPassword'=>$dbPassword);

							$dbConnection = $this->ConnectDb($dbinfo);

							foreach($orderNo as $o){
								$detailsInfo[] = $dbConnection->table('order_table')->where('rcp_no',$rcpNo)->where('order_number',$o->ord_No)->orwhereNull('PREPARE_DT')->orwhereNull('COLLECT_DT')->get();
							}		
							DB::purge('KPT'); 	

					}
											
				}
			}
			if(isset($detailsInfo)){
			return Response::json(array(
				'error' => false,
				'code' => 200,
				'data' => $detailsInfo
			));
			}else{
				return Response::json(array(
					'error' => true,
					'code' => 400,
					'message' => 'No order'
				));
			}
		}else{
			return Response::json(array(
				'error' => true,
				'code' => 400,
				'message' => 'No order'
			));
		}
	} */

	
}