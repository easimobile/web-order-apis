<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\ApiController as ApiController;
use Request;
use View;
use Session;
use Response;
use stdClass;
use Redirect;
use File;
use Validator;
use DB;
use Config;

class UserController extends BaseController
{
	public function __construct(){
		$this->apiCtrl = new ApiController();
		$this->outletCode = $this->apiCtrl->retrieveSysFlagSetting('AB1');
		$this->serviceCharge = $this->apiCtrl->retrieveSysFlagSetting2('S03');
		$this->gst = $this->apiCtrl->retrieveSysFlagSetting2('S04');
		$this->svcTaxType = $this->apiCtrl->retrieveSysFlagSetting2('S07');
		$this->gstTaxType = $this->apiCtrl->retrieveSysFlagSetting2('S08');
		$this->taxTableByPriceLvl = $this->apiCtrl->retrieveSysFlagSetting2('S12');
		$this->priceLevel = Config::get('defaults.priceLevel');
	}

	public function home(){
		$outletcode = $this->outletCode;
		
		if(strtoupper($this->taxTableByPriceLvl) == 'T'){		
			$pricelvl = $this->priceLevel;
			$tax = DB::connection('sqlsrv2')->table('pricelvl')->select('taxtype','tax_1','tax_2')->where('p_level',$pricelvl)->first();
			$_SESSION['taxType'] = $tax->taxtype;
			if(strtoupper($tax->taxtype) == 'E'){
				if($tax->tax_1 == 1){
					if($this->serviceCharge < 10){
						$svcRate = '0.0'.$this->serviceCharge;
						$_SESSION['svcRate'] = $svcRate;
					}else{
						$svcRate = '0.'.$this->serviceCharge;
						$_SESSION['svcRate'] = $svcRate;
					}
				}else{
					$_SESSION['svcRate'] = '0';
				}

				if($tax->tax_2 == 1){
					if($this->gst < 10){
						$gstRate = '0.0'.$this->gst;
						$_SESSION['gstRate'] = $gstRate;
					}else{
						$gstRate = '0.'.$this->gst;
						$_SESSION['gstRate'] = $gstRate;
					}
				}else{
					$_SESSION['gstRate'] = '0';
				}
			}else{
				if($this->serviceCharge < 10){
					$svcRate = '1.0'.$this->serviceCharge;
					$_SESSION['svcRate'] = $svcRate;
				}else{
					$svcRate = '1.'.$this->serviceCharge;
					$_SESSION['svcRate'] = $svcRate;
				}

				if($this->gst < 10){
					$gstRate = '1.0'.$this->gst;
					$_SESSION['gstRate'] = $gstRate;
				}else{
					$gstRate = '1.'.$this->gst;
					$_SESSION['gstRate'] = $gstRate;
				}
			}
		}else{
			if(strtoupper($this->svcTaxType) == 'E' && strtoupper($this->gstTaxType) == 'E'){
				$_SESSION['taxType'] = 'E';
			}else if(strtoupper($this->svcTaxType) == 'I' && strtoupper($this->gstTaxType) == 'I'){
				$_SESSION['taxType'] = 'I';
			}/* else{
				Session()->put('taxType', 'D');	
			} */
		
			if($this->svcTaxType == 'E'){
				if($this->serviceCharge < 10){
					$svcRate = '0.0'.$this->serviceCharge;
					$_SESSION['svcRate'] = $svcRate;
				}else{
					$svcRate = '0.'.$this->serviceCharge;
					$_SESSION['svcRate'] = $svcRate;
				}
			}else{
				if($this->serviceCharge < 10){
					$svcRate = '1.0'.$this->serviceCharge;
					$_SESSION['svcRate'] = $svcRate;
				}else{
					$svcRate = '1.'.$this->serviceCharge;
					$_SESSION['svcRate'] = $svcRate;
				}
			}

			if($this->gstTaxType == 'E'){
				if($this->gst < 10){
					$gstRate = '0.0'.$this->gst;
					$_SESSION['gstRate'] = $gstRate;
				}else{
					$gstRate = '0.'.$this->gst;
					$_SESSION['gstRate'] = $gstRate;
				}
			}else{
				if($this->gst < 10){
					$gstRate = '1.0'.$this->gst;
					$_SESSION['gstRate'] = $gstRate;
				}else{
					$gstRate = '1.'.$this->gst;
					$_SESSION['gstRate'] = $gstRate;
				}
			}
		}
		
		$tax1Name = $this->apiCtrl->retrieveSysFlagSetting2('N06');
		$tax2Name = $this->apiCtrl->retrieveSysFlagSetting2('N07');
		$gstTaxType = $this->gstTaxType;
		$products = $this->apiCtrl->getProducts($outletcode);
		$menu = $this->apiCtrl->getMenu($outletcode);

		if($products)
			return view::make('home_test',compact('products','menu','gstRate','tax1Name','tax2Name','gstTaxType'));		
	}
	
	public function index(){
		return View::make('login');
	}

	public function outlet(){
		$outlets = $this->apiCtrl->getOutlets();
		$outlets = $outlets->getData();
		$outlets = $outlets->data;
		if(count($outlets) > 0)
			return view::make('tableSelection_test',compact('outlets'));
		else{
			return view::make('tableSelection_test',compact('outlets'));
		}
	}

	public function apiauth(){
        $bytes = openssl_random_pseudo_bytes(64, $cstrong);
        $apikey = bin2hex($bytes);
        
        if(!$cstrong){
            return 0;
        } else {            
            return $apikey;
        }
	}

	public function error(){
		return view::make('error');
	}

	public function login(){
		if(Request::all()){
			$rules = array(
				'usercode'    => 'required',
				'password'    => 'required'
			);

			$validator = Validator::make(Request::all(), $rules);
			if($validator->passes()){	
				$userCode = Request::get('usercode');
				$password = Request::get('password');
				
				$user = DB::table('employee')->where('EMP_CD',$userCode)->where('Active',1)->first();		
				if(!$user){
					return Redirect::back()->with('fail','Invalid User ID/Password')->withInput(Request::except('password'));
				}
				
				$decrypted_pw = $this->apiCtrl->decryptPassword($user->EMP_PWD);
				if($decrypted_pw == $password){
					$apiKey = $this->apiauth();
					DB::table('apiAuth')->insert(array('userId'=>$userCode,'apikey'=>$apiKey));

					$_SESSION['empcd'] = $userCode;
					$_SESSION['apiKey'] = $apiKey;
				
					return redirect('tableselection');
				}else{		
					$encrytKeyedPassword = $this->apiCtrl->encryptPassword($password);
					return Redirect::back()->with('fail','Invalid User ID/Password')->withInput(Request::except('password'));
				}
			}else{
				return Redirect::back()->withErrors($validator)->withInput(Request::except('password'));		
			}
		}else{
        	return view::make('login');
        }
	}

	public function logout(){
		session_destroy();
		return redirect('login')->with('success',"Log Out Successfully");
	}
}